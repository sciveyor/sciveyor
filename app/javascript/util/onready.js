// Either bind fn() to the ready handler, or call it now if we're already loaded
export function onready(fn) {
  if (document.readyState != "loading") {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}
