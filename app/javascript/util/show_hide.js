// Return a selector for all elements that can take a 'disabled' class, within
// the current selector.
function formElementsFor(selector) {
  return [
    selector,
    selector + " div",
    selector + " label",
    selector + " input",
    selector + " select",
    selector + " textarea",
  ];

  return $(elements.join(","));
}

function setVisibleAndDisabled(selector, state) {
  for (var elt of document.querySelectorAll(selector)) {
    if (state) {
      elt.classList.remove("d-none");
    } else {
      elt.classList.add("d-none");
    }
  }

  for (var elt of document.querySelectorAll(formElementsFor(selector))) {
    if (state) {
      elt.classList.remove("disabled");
    } else {
      elt.classList.add("disabled");
    }
    elt.disabled = !state;
  }
}

export function hideAndDisable(selector) {
  setVisibleAndDisabled(selector, false);
}

export function showAndEnable(selector) {
  setVisibleAndDisabled(selector, true);
}

export function toggleVisAndDisabled(selector) {
  var visible = !document.querySelector(selector).classList.contains("d-none");
  setVisibleAndDisabled(selector, !visible);
}
