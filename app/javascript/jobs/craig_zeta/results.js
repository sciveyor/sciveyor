// ---------------------------------------------------------------------------
// Graph support for the CraigZeta results page
import { onready } from "util/onready";

function drawCraigZetaGraph() {
  // Get the elements we need
  var graphContainer = document.querySelector("#cz-graph");
  var tableContainer = document.querySelector("#cz-table");
  if (!graphContainer || !tableContainer) return;

  var results = JSON.parse(window.json_data);

  // Make a DataTable object for the graph
  var data = new google.visualization.DataTable();
  var rows = results.graph_points;

  data.addColumn("number", results.marker_1_header);
  data.addColumn("number", results.marker_2_header);
  data.addColumn({ type: "string", role: "tooltip" });
  data.addRows(rows);

  // Make the scatter chart object
  var w = document.querySelector("html").clientWidth;
  if (w > 750) {
    w = 750;
  }

  var h;
  if (w > 480) {
    h = 480;
  } else {
    h = w;
  }

  var options = {
    width: w,
    height: h,
    legend: { position: "none" },
    hAxis: { title: results.marker_1_header },
    vAxis: { title: results.marker_2_header },
    pointSize: 3,
  };

  var chart = new google.visualization.ScatterChart(graphContainer);
  chart.draw(data, options);
  google.visualization.events.trigger(chart, "updatelayout");

  // Make a DataTable object for the table
  data = new google.visualization.DataTable();
  rows = results.zeta_scores;

  // Add the data
  data.addColumn("string", results.word_header);
  data.addColumn("number", results.score_header);
  data.addRows(rows);

  // Make a pretty table object
  var table = new google.visualization.Table(tableContainer);
  table.draw(data, {
    page: "enable",
    pageSize: 20,
    sort: "disable",
    width: "20em",
  });
}

onready(function () {
  // Get the elements we need
  if (
    !document.querySelector("#cz-graph") ||
    !document.querySelector("#cz-table")
  )
    return;

  // Load the external libraries we need
  if (google.charts === undefined) return;
  google.charts.load("current", { packages: ["corechart", "table"] });
  google.charts.setOnLoadCallback(drawCraigZetaGraph);
});
