// ---------------------------------------------------------------------------
// Support for the "all" checkbox to toggle the "number of X" input to
// disabled
//
// This relies on some weird internal stuff, so I'm documenting it in some
// detail. The event triggers on an input. That input is contained in a
// div.form-check (checkbox) which is contained in a div.mb-3 (check group).
//
// The previous sibling of that mb-3 is going to be the thing that contains
// all of the controls we want to activate or deactivate (the "number of X"
// input and its label).

document.addEventListener("change", function (e) {
  if (e.target && e.target.getAttribute("name") == "job_params[all]") {
    var checked = e.target.checked;

    var check_group = e.target.parentNode.parentNode;
    var number_group = check_group.previousElementSibling;

    var number_inputs = number_group.querySelectorAll("input");

    if (checked) {
      for (var input of number_inputs) {
        input.classList.add("disabled");
        input.disabled = true;
      }
    } else {
      for (var input of number_inputs) {
        input.classList.remove("disabled");
        input.disabled = false;
      }
    }
  }
});
