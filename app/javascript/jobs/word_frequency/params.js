// ---------------------------------------------------------------------------
// Support for the dependencies between the form controls on the params page
import {
  toggleVisAndDisabled,
  showAndEnable,
  hideAndDisable,
} from "util/show_hide";

document.addEventListener("change", function (e) {
  if (!e.target) return;

  if (e.target.getAttribute("id") == "job_params_ngram_method") {
    // If we've shown the single controls, then reset the sub-control
    // visibility, otherwise hide the sub-controls
    var event = document.createEvent("HTMLEvents");
    event.initEvent("change", true, false);

    if (e.target.value == "single") {
      showAndEnable("#single-controls");
      hideAndDisable("#ngram-controls");

      document.querySelector("#job_params_word_method").dispatchEvent(event);
      document.querySelector("#job_params_exclude_method").dispatchEvent(event);
    } else {
      hideAndDisable("#single-controls");
      showAndEnable("#ngram-controls");

      hideAndDisable("#num-words-controls");
      hideAndDisable("#inclusion-list-controls");
      document.querySelector("#job_params_all").dispatchEvent(event);
      document.querySelector("#job_params_exclude_method").dispatchEvent(event);
    }
  }

  if (e.target.getAttribute("id") == "job_params_block_method") {
    toggleVisAndDisabled("#count-controls");
    toggleVisAndDisabled("#blocks-controls");
  }

  if (e.target.getAttribute("id") == "job_params_word_method") {
    var option = e.target.value;

    if (option == "count") {
      showAndEnable("#num-words-controls");
      hideAndDisable("#inclusion-list-controls");
      showAndEnable("#exclusion-controls");
    } else if (option == "list") {
      hideAndDisable("#num-words-controls");
      showAndEnable("#inclusion-list-controls");
      hideAndDisable("#exclusion-controls");
    } else {
      hideAndDisable("#num-words-controls");
      hideAndDisable("#inclusion-list-controls");
      showAndEnable("#exclusion-controls");
    }
  }

  if (e.target.getAttribute("id") == "job_params_exclude_method") {
    var option = e.target.value;

    if (option == "common") {
      showAndEnable("#exclude-common-controls");
      hideAndDisable("#exclude-list-controls");
      hideAndDisable("#exclude-list-ngram-controls");
    } else if (option == "list") {
      hideAndDisable("#exclude-common-controls");
      if (
        document.getElementById("job_params_ngram_method").value == "single"
      ) {
        showAndEnable("#exclude-list-controls");
        hideAndDisable("#exclude-list-ngram-controls");
      } else {
        showAndEnable("#exclude-list-ngram-controls");
        hideAndDisable("#exclude-list-controls");
      }
    } else {
      hideAndDisable("#exclude-common-controls");
      hideAndDisable("#exclude-list-controls");
      hideAndDisable("#exclude-list-ngram-controls");
    }
  }
});
