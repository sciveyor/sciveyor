// ---------------------------------------------------------------------------
// Support for drawing an interactive, configurable word cloud in D3
import { select, selectAll } from "d3-selection";
import cloud from "d3-cloud";
import { scaleSequential } from "d3-scale";
import {
  interpolateGreens,
  interpolateReds,
  interpolateGreys,
  interpolateOranges,
  interpolatePurples,
  interpolateBlues,
} from "d3-scale-chromatic";
import { transition } from "d3-transition";
import { onready } from "util/onready";
import { shuffle } from "util/shuffle";

function setWordRotation(words, rotate) {
  for (var i = 0; i < words.length; i++) {
    if (rotate) {
      words[i].rotate = (~~(Math.random() * 6) - 3) * 30;
    } else {
      words[i].rotate = 0;
    }
  }
}

function setWordColor(words, color) {
  var numWords = words.length;
  var interpolator;

  switch (color) {
    case "greens":
      interpolator = interpolateGreens;
      break;
    case "reds":
      interpolator = interpolateReds;
      break;
    case "greys":
      interpolator = interpolateGreys;
      break;
    case "oranges":
      interpolator = interpolateOranges;
      break;
    case "purples":
      interpolator = interpolatePurples;
      break;
    default:
      interpolator = interpolateBlues;
      break;
  }

  var colorScale = scaleSequential(interpolator).domain([0, numWords]);

  var colorPositions = [];
  for (var i = 0; i < numWords; i++) {
    colorPositions.push(i);
  }
  colorPositions = shuffle(colorPositions);

  for (var i = 0; i < numWords; i++) {
    words[i].color = colorScale(colorPositions[i]);
  }
}

function setWordFont(words, font) {
  for (var i = 0; i < words.length; i++) {
    words[i].font = font;
  }
}

function redrawWordCloud(layout, newWords) {
  layout.stop().words(newWords).start();
}

function toggleWordCloudRotation(container) {
  var layout = container.sciveyorLayout;
  var rotated = container.sciveyorRotated;

  var newWords = layout.words();

  setWordRotation(newWords, !rotated);
  container.sciveyorRotated = !rotated;
  redrawWordCloud(layout, newWords);
}

function setWordCloudColor(container, color) {
  var layout = container.sciveyorLayout;
  var newWords = layout.words();

  setWordColor(newWords, color);
  redrawWordCloud(layout, newWords);
}

function setWordCloudFont(container, font) {
  var layout = container.sciveyorLayout;
  var newWords = layout.words();

  setWordFont(newWords, font);
  redrawWordCloud(layout, newWords);
}

function drawWordCloud(words, extents) {
  var vis = this.sciveyorVis;
  var layout = this.sciveyorLayout;

  var text = vis.selectAll("text").data(words);
  var size = layout.size();

  // Automatically scale to keep the words visible
  var scale = 1;
  if (extents) {
    scale =
      Math.min(
        size[0] / Math.abs(extents[1].x - size[0] / 2),
        size[0] / Math.abs(extents[0].x - size[0] / 2),
        size[1] / Math.abs(extents[1].y - size[1] / 2),
        size[1] / Math.abs(extents[0].y - size[1] / 2)
      ) / 2;
  }

  // Animated transitions for font size, transform position, color
  text
    .transition()
    .duration(1e3)
    .attr("transform", function (d) {
      return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
    })
    .style("font-size", function (d) {
      return d.size + "px";
    })
    .style("fill", function (d) {
      return d.color;
    });

  // Switch font family directly
  text.style("font-family", function (d) {
    return d.font;
  });

  // Set all the relevant attributes on construction
  text
    .enter()
    .append("text")
    .text(function (d) {
      return d.text;
    })
    .attr("text-anchor", "middle")
    .attr("transform", function (d) {
      return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
    })
    .style("font-family", function (d) {
      return d.font;
    })
    .style("fill", function (d) {
      return d.color;
    })
    .style("font-size", "1px")
    .transition()
    .duration(1e3)
    .style("font-size", function (d) {
      return d.size + "px";
    });

  // Animated transition for scaling
  vis
    .transition()
    .delay(1e3)
    .duration(750)
    .attr(
      "transform",
      "translate(" + [size[0] / 2, size[1] / 2] + ")scale(" + scale + ")"
    );
}

function setupWordCloud(container) {
  var words = JSON.parse(container.getAttribute("data-sv-word-cloud"));
  var numWords = Object.keys(words).length;

  var containerWidth = container.clientWidth;
  var size = [containerWidth, containerWidth * 0.7];

  // Build elements
  var svg = select(container)
    .append("svg")
    .attr("width", size[0])
    .attr("height", size[1]);
  var vis = svg
    .append("g")
    .attr("transform", "translate(" + size[0] / 2 + "," + size[1] / 2 + ")");

  container.sciveyorSVG = svg;
  container.sciveyorVis = vis;
  container.sciveyorRotated = true;

  // Normalize the words to the largest one present
  var max = 0,
    min = 9999;
  for (var [word, count] of Object.entries(words)) {
    if (count > max) {
      max = count;
    }
    if (count < min) {
      min = count;
    }
  }

  // Build the word data
  var wordData = Object.keys(words).map(function (w) {
    return {
      text: w,
      size: ((words[w] - min) / (max - min)) * 90 + 10,
    };
  });

  setWordColor(wordData, "blues");
  setWordRotation(wordData, true);
  setWordFont(wordData, "sans-serif");

  // Start the layout engine
  var layout = cloud()
    .size(size)
    .words(wordData)
    .rotate(function (d) {
      return d.rotate;
    })
    .font(function (d) {
      return d.font;
    })
    .fontSize(function (d) {
      return d.size;
    })
    .on("end", drawWordCloud.bind(container));

  container.sciveyorLayout = layout;
  layout.start();

  // Hook up our controls
  container
    .querySelector(".word-cloud-font")
    .addEventListener("blur", function (e) {
      setWordCloudFont(container, e.target.value);
    });
  container
    .querySelector(".word-cloud-color")
    .addEventListener("change", function (e) {
      setWordCloudColor(container, e.target.value);
    });
  container
    .querySelector(".word-cloud-rotate")
    .addEventListener("click", function (e) {
      toggleWordCloudRotation(container);
    });
  container
    .querySelector(".word-cloud-download")
    .addEventListener("click", function (e) {
      var serializer = new XMLSerializer();

      select(this)
        .attr(
          "href",
          "data:application/octet-stream;base64," +
            btoa(serializer.serializeToString(container.sciveyorSVG.node()))
        )
        .attr("download", "word_cloud.svg");
    });
}

onready(function () {
  for (var cloud of document.querySelectorAll(".word-cloud")) {
    setupWordCloud(cloud);
  }
});
