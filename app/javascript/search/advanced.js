// ---------------------------------------------------------------------------
// Row building support for search#advanced
import { onready } from "util/onready";
import { appendFromHTML } from "util/from_html";
import { showAndEnable, hideAndDisable } from "util/show_hide";

function setAdvancedButtons(root) {
  for (var button of root.querySelectorAll(".advanced-add-button")) {
    button.addEventListener("click", function (e) {
      addSearchAdvancedRow(e.target);
    });
  }
  for (var button of root.querySelectorAll(".advanced-remove-button")) {
    button.addEventListener("click", function (e) {
      removeSearchAdvancedRow(e.target);
    });
  }
}

function updateAdvancedRows() {
  if (!document.querySelectorAll("#advanced-rows")) return;

  // Show the boolean toggles on every row but the last one
  showAndEnable(".advanced-row .bool-group select");
  hideAndDisable(".advanced-row:last-of-type .bool-group select");

  // Show all the minus buttons
  for (var button of document.querySelectorAll(".advanced-remove-button")) {
    button.classList.add("visible");
    button.classList.remove("invisible");
  }

  // Hide all but the last plus button
  var add_buttons = document.querySelectorAll(".advanced-add-button");
  for (var button of add_buttons) {
    button.classList.add("invisible");
    button.classList.remove("visible");
  }
  add_buttons[add_buttons.length - 1].classList.add("visible");
  add_buttons[add_buttons.length - 1].classList.remove("invisible");

  // If there's only one row, nothing gets minus buttons
  if (document.querySelectorAll(".advanced-row").length == 1) {
    for (var button of document.querySelectorAll(".advanced-remove-button")) {
      button.classList.add("invisible");
      button.classList.remove("visible");
    }
  }
}

function addSearchAdvancedRow(button) {
  var container = document.querySelector("#advanced-rows");
  if (!container) return;

  var row = appendFromHTML(container, window.rlRowMarkup);
  setAdvancedButtons(row);
  updateAdvancedRows();
}

function removeSearchAdvancedRow(button) {
  var row = button.closest(".advanced-row");
  row.parentNode.removeChild(row);

  updateAdvancedRows();
}

/*
function fieldDropdownChange(event) {
  var field = e.target;
  if (!field || !field.parentNode || !field.parentNode.parentNode) return;

  var row = e.target.parentNode.parentNode;
  if (e.target.nodeName != "select" || !row.classList.contains("advanced-row"))
    return;

  var selected = e.target.selectedOptions[0];

  // See if the row has a typeahead attached or not
  if (row.sciveyorTypeaheadActive === undefined)
    row.sciveyorTypeaheadActive = false;
  var active = row.getAttribute("typeahead-active");

  // See if we need one or not
  var val = option.value;
  var need_active = val == "authors" || val == "journal_exact";

  // If we don't have one and don't need it, we're done
  if (!active && !need_active) return;

  var input = row.querySelector(".value-group input");

  // If it's active, destroy it regardless
  if (active) {
    // Destroy the typeahead
    input.typeahead("destroy");
    row.sciveyorTypeaheadActive = false;
  }

  // Create one if we need to
  if (!need_active) return;

  var bloodhound, name;

  if (val == "authors") {
    name = "authors";
    bloodhound = window.rlBloodhoundAuthors;
  } else {
    name = "journals";
    bloodhound = window.rlBloodhoundJournals;
  }

  input.typeahead(
    { highlight: true },
    {
      name: name,
      displayKey: "val",
      source: bloodhound.ttAdapter(),
    }
  );
  row.sciveyorTypeaheadActive = true;
}

function createAutocompleteSystem() {
  container = document.querySelector("#advanced-rows");
  if (!container) return;

  // Create a pair of datasets for typeahead
  window.rlBloodhoundAuthors = new Bloodhound({
    name: "authors",
    remote: {
      url: "/lists/authors?q=%QUERY",
      wildcard: "%QUERY",
    },
    datumTokenizer: function (d) {
      Bloodhound.tokenizers.nonword(d.val);
    },
    queryTokenizer: Bloodhound.tokenizers.nonword,
  });
  window.rlBloodhoundJournals = new Bloodhound({
    name: "journals",
    remote: {
      url: "/lists/journals?q=%QUERY",
      wildcard: "%QUERY",
    },
    datumTokenizer: function (d) {
      Bloodhound.tokenizers.whitespace(d.val);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
  });

  window.rlBloodhoundAuthors.initialize();
  window.rlBloodhoundJournals.initialize();

  // Hook the change events to create and destroy typeaheads
  document.addEventListener("change", fieldDropdownChange);
}
*/
onready(function () {
  updateAdvancedRows();
  //createAutocompleteSystem();
  setAdvancedButtons(document);
});
