// ---------------------------------------------------------------------------
import InfiniteScroll from "infinite-scroll";
import { onready } from "util/onready";

onready(function () {
  // Infinite scroll support for search#index
  var infScroll = new InfiniteScroll("#scroller", {
    path: function () {
      var links = document.querySelectorAll("a.scroll-next-page");
      var href = links[links.length - 1].getAttribute("href");

      // Replace the (user) search path with the (ajax) load path
      return href.replace("/search", "/search/load");
    },
    append: "table.document-list",
    loadOnScroll: true,
    history: false,
    fetchOptions: {
      headers: {
        Accept: "text/html",
      },
    },
  });

  infScroll.on("append", function () {
    var containers = document.querySelectorAll(".scroll-pagination");
    for (var row of containers) {
      row.classList.add("d-none");
    }
  });

  // Show a "back to top" button if we're scrolled off the top
  var back_button = document.querySelector("#back-to-top");
  if (back_button) {
    document.addEventListener("scroll", function (e) {
      var y = window.scrollY;
      if (y > 800) back_button.classList.remove("d-none");
      else back_button.classList.add("d-none");
    });
  }

  // Update the URL in the dataset-add button as we use the form
  document.addEventListener("change", function (e) {
    if (e.target && e.target.classList.contains("search-add-select")) {
      // Get the form parent
      var form = e.target.closest("form");
      var url = "/datasets/" + e.target.value + "/";
      form.setAttribute("action", url);
    }
  });
});
