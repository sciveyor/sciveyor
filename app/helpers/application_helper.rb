# frozen_string_literal: true

module ApplicationHelper
  # Return a form object decorated with our builder
  #
  # @param [Hash] args the arguments for `form_with`
  # @yield Passes a form builder to the block
  # @yieldparam [FormBuilder] form the FormBuilder object
  def sciveyor_form_with(
    model: nil,
    scope: nil,
    url: nil,
    format: nil,
    **options,
    &block
  )
    options ||= {}
    options[:builder] = Sciveyor::Presenters::BootstrapFormBuilder
    form_with model: model,
              scope: scope,
              url: url,
              format: format,
              **options,
              &block
  end

  # Standard markup for a close icon
  #
  # @param [Hash] data if set, data attributes for the tag
  # @return [String] the close icon
  def close_icon(data = nil)
    attributes = {
      class: "btn-close",
      type: "button",
      "aria-label": I18n.t("common.close")
    }
    data&.each { |k, v| attributes["data-#{k}"] = v }

    content_tag(:button, "", attributes)
  end
end
