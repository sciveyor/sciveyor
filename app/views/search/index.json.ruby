# frozen_string_literal: true

ret = { 'results' => { 'num_hits' => @result.num_hits } }

if @result.solr_response.dig('responseHeader', 'params')
  ret['results']['solr_params'] =
    @result.solr_response['responseHeader']['params']
end

if @result.facets&.all&.present?
  ret['results']['facets'] =
    @result
      .facets
      .all
      .map do |f|
        {
          'field' => f.field,
          'value' => f.value,
          'query' => f.query,
          'hits' => f.hits,
        }
      end
end

ret['results']['documents'] =
  @result.documents.map do |d|
    {
      'schema' => d.schema,
      'version' => d.version,
      'id' => d.id,
      'doi' => d.doi,
      'externalIds' => d.externalIds,
      'license' => d.license,
      'licenseUrl' => d.licenseUrl,
      'dataSource' => d.dataSource,
      'dataSourceUrl' => d.dataSourceUrl,
      'dataSourceVersion' => d.dataSourceVersion,
      'type' => d.type,
      'title' => d.title,
      'journal' => d.journal,
      'date' => d.date&.iso8601,
      'dateElectronic' => d.dateElectronic&.iso8601,
      'dateAccepted' => d.dateAccepted&.iso8601,
      'dateReceived' => d.dateReceived&.iso8601,
      'volume' => d.volume,
      'number' => d.number,
      'pages' => d.pages,
      'keywords' => d.keywords,
      'tags' => d.tags,
      'authors' =>
        d.authors.map do |a|
          {
            'name' => a.name,
            'first' => a.first,
            'middle' => a.middle,
            'last' => a.last,
            'prefix' => a.prefix,
            'suffix' => a.suffix,
            'affiliation' => a.affiliation,
          }
        end,
    }
  end

ret.to_json
