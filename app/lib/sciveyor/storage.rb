# frozen_string_literal: true

module Sciveyor
  # Code for storing, retrieving, and deleting files in remote storage.
  #
  # In order to communicate between the various worker and server processes of
  # Sciveyor, we need a kind of remote storage that is easy to use, clearly
  # documented, and doesn't do much. This module implements a very lightweight
  # wrapper around either S3 (in production) or disk (in development or test)
  # file storage that only works on unique filenames as keys.
  module Storage
    # Exception thrown on failure to execute a file operation
    class Error < RuntimeError
    end

    # Upload a file to remote storage
    #
    # This command sends a file to S3. It will raise an error if we are unable
    # to successfully send the file, or if a file with this name already exists
    # on the remote server.
    #
    # @param [IO] io the file data to upload (can be a StringIO)
    # @param [String] filename the filename to save this file under (must be
    #   unique in the bucket)
    # @raise [Sciveyor::Storage::Error] if the filename already exists, or if
    #   there is a network error in communication
    # @return [void]
    def self.upload(io, filename)
      if local_storage?
        temp_file = get_temp_file(filename)
        raise Error.new("file already exists") if File.exist?(temp_file)

        File.open(temp_file, "w") { |out| IO.copy_stream(io, out) }
        return
      end

      client = Aws::S3::Client.new

      # See if the object already exists
      begin
        client.head_object(bucket: ENV["S3_BUCKET"], key: filename)
        raise Error.new("file already exists")
      rescue Aws::S3::Errors::NotFound
        # This is good; this will be raised if the file does not exist
      rescue Aws::S3::Errors::ServiceError => e
        # This is bad, it means we couldn't check whether the file exists
        raise Error.new("could not check for duplicate file: " + e.to_s)
      end

      # Upload it
      md5 = Digest::MD5.base64digest(io.read)
      io.rewind

      type = Marcel::MimeType.for(io, name: filename).to_s
      io.rewind

      begin
        client.put_object(
          bucket: ENV["S3_BUCKET"],
          key: filename,
          body: io,
          content_type: type,
          content_md5: md5
        )
      rescue Aws::S3::Errors::ServiceError => e
        raise Error.new("putting object failed: " + e.to_s)
      end
    end

    # Delete a file from remote storage
    #
    # Attempt to delete a file from S3. Note that if the file does not exist on
    # the remote side, we will not raise an error and simply fail silently.
    #
    # @param [String] filename the filename to delete
    # @return [void]
    def self.delete(filename)
      return if filename.nil?

      if local_storage?
        temp_file = get_temp_file(filename)
        File.unlink(temp_file) if File.exist?(temp_file)
        return
      end

      begin
        client = Aws::S3::Client.new
        client.delete_object(bucket: ENV["S3_BUCKET"], key: filename)
      rescue Aws::S3::Errors::ServiceError
        # Silently swallow any AWS errors, we simply don't care
      end
    end

    # Yield an IO for a file in remote storage
    #
    # This function will download a file from remote storage, save it in a
    # temporary folder, and then yield an IO on the file contents to the
    # passed block, along with the content type. The temporary file will be
    # deleted when the block returns.
    #
    # @param [String] filename the filename to get
    # @yield [io, content_type] Passes the file contents to the block
    # @yieldparam [IO] io the contents of the given file
    # @yieldparam [String] content_type the content_type of the file
    # @raise [Sciveyor::Storage::Error] if the file does not exist, or if there
    #   is a network error in communication
    # @return [void]
    def self.get(filename)
      if local_storage?
        temp_file = get_temp_file(filename)
        raise Error.new("file does not exist") unless File.exist?(temp_file)

        File.open(temp_file, "r") do |f|
          yield(f, Marcel::MimeType.for(f, name: filename).to_s)
        end
        return
      end

      resp = nil
      begin
        client = Aws::S3::Client.new
        resp = client.get_object(bucket: ENV["S3_BUCKET"], key: filename)
      rescue Aws::S3::Errors::NotFound
        raise Error.new("could not find file")
      rescue Aws::S3::Errors::ServiceError => e
        raise Error.new("could not get file: " + e.to_s)
      end

      temp_file = get_temp_file(filename)
      IO.copy_stream(resp.body, temp_file)

      File.open(temp_file, "r") { |f| yield(f, resp.content_type) }

      File.unlink(temp_file)
    end

    # @return [Boolean] true if we should use local storage
    private_class_method def self.local_storage?
      return false if ENV["TEST_S3"].present?
      Rails.env.test? || Rails.env.development?
    end

    # Return the temporary file path for file-based storage
    # @param [String] filename the filename to build
    # @return [String] the temporary filename to use
    private_class_method def self.get_temp_file(filename)
      if Rails.env.test?
        Rails.root.join("tmp", "test_storage", filename)
      else
        Rails.root.join("tmp", "storage", filename)
      end
    end
  end
end
