# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Compare two datasets using the Craig Zeta algorithm
    class CraigZeta < Base
      include Sciveyor::Visualization::Csv

      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["CRAIG_ZETA_JOB_DISABLED"] || "false").to_boolean
      end

      # Return how many datasets this job requires
      #
      # @return [Integer] number of datasets needed to perform this job
      def self.num_datasets
        2
      end

      # Determine which words mark out differences between two datasets.
      #
      # This saves its data out as a CSV file to be downloaded by the user
      # later.  As of yet, we don't offer display in the browser; I think this
      # data is so complex that you'll want to pull it up on a spreadsheet.
      #
      # @param [Task] task the task we're working from
      # @return [void]
      def perform(task, options = {})
        super

        unless datasets.size == 2
          raise ArgumentError, "Wrong number of other datasets provided"
        end

        # Get the data
        analyzer =
          Sciveyor::Analysis::CraigZeta.call(
            dataset_1: datasets[0],
            dataset_2: datasets[1]
          )

        # Swap out the UUIDs in the graph point labels for friendly names
        analyzer.graph_points.each do |p|
          p.name =
            p.name.gsub(
              "#{datasets[0].uuid}:",
              "#{datasets[0].name_for(user)}:"
            )
          p.name =
            p.name.gsub(
              "#{datasets[1].uuid}:",
              "#{datasets[1].name_for(user)}:"
            )
        end

        # Save out all the data
        csv_string =
          csv_with_header(
            header:
              t(
                ".csv_header",
                name_1: datasets[0].name_for(user),
                name_2: datasets[1].name_for(user)
              )
          ) do |csv|
            # Output the marker words
            write_csv_data(
              csv: csv,
              data: analyzer.dataset_1_markers.zip(analyzer.dataset_2_markers),
              data_spec: {
                t(".marker_header", name: datasets[0].name_for(user)) => :first,
                t(".marker_header", name: datasets[1].name_for(user)) => :second
              }
            )
            csv << [""] << [""]

            # Output the graphing points
            csv << [t(".graph_header")]
            csv << [""]
            write_csv_data(
              csv: csv,
              data: analyzer.graph_points,
              data_spec: {
                t(".marker_column", name: datasets[0].name_for(user)) => :x,
                t(".marker_column", name: datasets[1].name_for(user)) => :y,
                t(".block_name_column") => :name
              }
            )
            csv << [""] << [""]

            # Output the Zeta scores
            csv << [t(".zeta_score_header")]
            csv << [""]
            write_csv_data(
              csv: csv,
              data: analyzer.zeta_scores,
              data_spec: {
                t(".word_column") => :first,
                t(".score_column") => :second
              }
            )
          end

        # Only make relatively small word clouds
        word_cloud_size = analyzer.dataset_1_markers.size
        word_cloud_size = 50 if word_cloud_size > 50

        data = {
          name_1: datasets[0].name_for(user),
          name_2: datasets[1].name_for(user),
          markers_1: analyzer.dataset_1_markers,
          markers_2: analyzer.dataset_2_markers,
          graph_points: analyzer.graph_points.map(&:to_a),
          zeta_scores: analyzer.zeta_scores.to_a,
          marker_1_header:
            t(".marker_column", name: datasets[0].name_for(user)),
          marker_2_header:
            t(".marker_column", name: datasets[1].name_for(user)),
          word_header: t(".word_column"),
          score_header: t(".score_column"),
          word_cloud_1_words: Hash[analyzer.zeta_scores.take(word_cloud_size)],
          word_cloud_2_words:
            Hash[analyzer.zeta_scores.reverse_each.take(word_cloud_size)]
        }

        # Write it out
        task
          .uploads
          .create(
            filename: unique_filename("craig_zeta.json"),
            content_type: "application/json",
            description: "Raw JSON Data",
            short_description: "JSON"
          ) { |f| f.from_string(data.to_json) }

        task
          .uploads
          .create(
            filename: unique_filename("results.csv"),
            content_type: "text/csv",
            description: "Spreadsheet",
            short_description: "CSV",
            downloadable: true
          ) { |f| f.from_string(csv_string) }

        task.mark_completed
      end
    end
  end
end
