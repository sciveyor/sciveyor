# frozen_string_literal: true

module Sciveyor
  # Module containing all code for running jobs
  module Jobs
    # Base class for all jobs in the application
    #
    # When a user requests that we perform a task, the worker process will
    # run code encapsulated in a class that inherits from this one.
    class Base
      # Returns true if this job can be run right now
      #
      # This method is usually implemented in order to (1) check for a feature
      # flag environment variable which can be set to disable jobs, and (2)
      # to make sure that any required external tools or libraries that a job
      # needs are installed. It defaults to true in the base class, and should
      # be overridden.
      #
      # @return [Boolean] true if this job can be run now
      def self.available?
        true
      end

      # Return how many datasets this job requires
      #
      # This method defaults to 1 in the base class, and can be overridden by
      # jobs which require more than one dataset to operate.
      #
      # @return [Integer] number of datasets needed to perform this job
      def self.num_datasets
        1
      end

      # Returns a translated string for this job
      #
      # This handles the i18n scoping for job classes.  It will
      # pass fully scoped translation keys along unaltered.
      #
      # @param [String] key the translation to look up (e.g., '.short_desc')
      # @param [Hash] opts the options for the translation
      # @return [String] the translated message
      def self.t(key, opts = {})
        return I18n.t(key, **opts) unless key[0] == "."

        I18n.t(
          "#{name.underscore.tr("/", ".").delete_prefix("sciveyor.")}#{key}",
          **opts
        )
      end

      # Returns a translated string for this job
      #
      # We alias the class method as an instance method, to save keystrokes
      # when programming job classes.
      #
      # @param [String] key the translation to look up (e.g., '.short_desc')
      # @return [String] the translated message
      def t(key, opts = {})
        self.class.t(key, **opts)
      end

      # Method called to perform a job
      #
      # This will perform some default handling; every class that derives from
      # this one should start by calling `super`.
      #
      # @param [Task] task the task on which we're working
      # @param [Hash] options the options passed to the job
      def perform(task, options)
        # Save some of our details
        @task = task
        @datasets = task&.datasets

        @options = options
        @options.symbolize_keys! if @options.respond_to?(:symbolize_keys!)
      end

      protected

      # Access the task
      #
      # @return [Task] the task we're working on
      def task
        @task
      end

      # Access the dataset
      #
      # @return [Dataset] the dataset we are working on
      def dataset
        if @datasets.count > 1
          raise ArgumentError, "task was created with more than one dataset"
        end

        @datasets.first
      end

      # Access the datasets (if created with more than one)
      #
      # @return [Array<Dataset>] the array of datasets we are working on
      def datasets
        if @datasets.count == 1
          raise ArgumentError, "task was only created with one dataset"
        end

        @datasets
      end

      # Access the user
      #
      # @return [User] the user whose task we are working on
      def user
        @task.user
      end

      # Get a unique filename for this job
      #
      # We want to have moderately user-readable filenames that still are unique
      # so that we don't have collisions on S3. Generate those here.
      #
      # @param [String] base the base filename to start with, e.g., `terms.json`
      # @return [String] the unique filename, e.g.,
      #   `base-V1StGXR8_Z5jdHi6B-myT.json`
      def unique_filename(base)
        ext = File.extname(base)
        base = File.basename(base, ext)

        base + "-" + Nanoid.generate + ext
      end
    end
  end
end
