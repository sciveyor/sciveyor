# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Determine statistically significant cooccurrences in text
    class Cooccurrence < Base
      include Sciveyor::Visualization::Csv

      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["COOCCURRENCE_JOB_DISABLED"] || "false").to_boolean
      end

      # Locate significant associations between words at distance.
      #
      # This saves its data out as a CSV file to be downloaded by the user
      # later.
      #
      # @param [Task] task the task we're working from
      # @param [Hash] options parameters for this job
      # @option options [String] :scoring the algorithm to use to
      #   analyze the significance of coocurrences.  Can be `'mutual_information'`,
      #   `'log_likelihood'`, or `'t_test'`.
      # @option options [String] :num_pairs number of coccurrences to return
      # @option options [String] :stemming the stemming method to use. Can be
      #   +:stem+, +:lemma+, or +:no+.
      # @option options [String] :window the window for searching for
      #   cooccurrences
      # @option options [String] :words the words to search for cooccurrences
      #   with
      # @return [void]
      def perform(task, options)
        super
        @options.delete(:stemming) if @options[:stemming] == "no"

        result =
          Sciveyor::Analysis::Cooccurrence.call(
            @options.merge(dataset: dataset)
          )

        case result.scoring
        when :mutual_information
          algorithm = t("common.scoring.mutual_information")
          column = t("common.scoring.mutual_information_header")
        when :t_test
          algorithm = t("common.scoring.t_test")
          column = t("common.scoring.t_test_header")
        when :log_likelihood
          algorithm = t("common.scoring.log_likelihood")
          column = t("common.scoring.log_likelihood_header")
        end

        # Save out all the data
        csv_string =
          csv_with_header(
            header: t(".header", name: dataset.name_for(user)),
            subheader: t(".subheader", test: algorithm)
          ) do |csv|
            write_csv_data(
              csv: csv,
              data: result.cooccurrences,
              data_spec: {
                t(".pair") => :first,
                column => :second
              }
            )
          end

        # Write out the CSV to a file
        task
          .uploads
          .create(
            filename: unique_filename("results.csv"),
            content_type: "text/csv",
            description: "Spreadsheet",
            short_description: "CSV",
            downloadable: true
          ) { |f| f.from_string(csv_string) }
        task.mark_completed
      end

      # Helper method for creating the job parameters view
      #
      # This method returns the list of available significance test methods
      # along with their translated user-friendly names, useful for looping
      # over to display them for the user to choose from.
      #
      # @return [Array<(String, Symbol)>] pairs of test method names and their
      #   associated symbols
      def self.significance_tests
        %i[mutual_information t_test log_likelihood].map do |sym|
          [t("common.scoring.#{sym}"), sym]
        end
      end
    end
  end
end
