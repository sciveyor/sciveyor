# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Export all of the data belonging to a user
    class UserExport < Base
      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["USER_EXPORT_JOB_DISABLED"] || "false").to_boolean
      end

      # Export all of a user's data as a ZIP file
      #
      # @param [User] user the user whose data we want to export
      # @return [void]
      def perform(task, options = {})
        super

        # Kill any already attached file
        if user.export_filename.present?
          Sciveyor::Storage.delete(user.export_filename)
        end

        # All of this goes into a ZIP file
        ios =
          ::Zip::OutputStream.write_buffer(StringIO.new) do |zos|
            # Every user column, straight off the schema
            user_data = {
              name: user.name,
              email: user.email,
              created_at: user.created_at.to_s,
              updated_at: user.updated_at.to_s,
              language: user.language,
              timezone: user.timezone,
              encrypted_password: user.encrypted_password,
              reset_password_token: user.reset_password_token,
              reset_password_sent_at: user.reset_password_sent_at.to_s,
              remember_created_at: user.remember_created_at.to_s,
              workflow_active: user.workflow_active,
              workflow_class: user.workflow_class,
              workflow_datasets: user.workflow_datasets.to_s,
              tasks: []
            }

            # Tasks
            uploads_to_save = []

            user.tasks.each do |t|
              # Don't export the user-export task we're running
              next if t == task

              task_out = {
                name: t.name,
                datasets: t.datasets.map { |d| d.uuid },
                finished_at: t.finished_at.to_s,
                created_at: t.created_at.to_s,
                updated_at: t.updated_at.to_s,
                finished: t.finished,
                failed: t.failed,
                type: t.type,
                job_message: t.job_message,
                job_params: t.job_params,
                uploads: []
              }

              t.uploads.each do |f|
                unless f.filename
                  task_out[:uploads] << "<empty file record>"
                  next
                end

                uploads_to_save << f.filename
                task_out[:uploads] << f.filename
              end

              user_data[:tasks] << task_out
            end

            # Serialize it to the ZIP
            zos.put_next_entry("user.json")
            zos.print user_data.to_json

            # Libraries
            libraries = []
            user.libraries.each do |l|
              library = { name: l.name, url: l.url }

              libraries << library
            end

            zos.put_next_entry("libraries.json")
            zos.print libraries.to_json

            # Datasets
            datasets = []

            user.datasets_hash.each do |name, d|
              dataset = {
                name: name,
                uuid: d.uuid,
                document_count: d.document_count.to_s,
                q: d.q,
                boolean: d.boolean,
                fq: d.fq
              }

              datasets << dataset
            end

            zos.put_next_entry("datasets.json")
            zos.print datasets.to_json

            # And all of the uploads
            uploads_to_save.each do |filename, file|
              begin
                Sciveyor::Storage.get(filename) do |io, type|
                  zos.put_next_entry(filename)
                  zos.write(io.read)
                end
              rescue Sciveyor::Storage::Error
                # Don't worry about errors here, just don't add the file
                next
              end
            end
          end

        # Save it into the user object, and they'll be able to download it
        ios.rewind

        user.export_filename = "export_#{Nanoid.generate}.zip"
        user.export_content_type = "application/zip"

        Sciveyor::Storage.upload(ios, user.export_filename)

        user.save

        task.mark_completed
      end
    end
  end
end
