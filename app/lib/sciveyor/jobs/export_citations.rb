# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Export a dataset in a given citation format
    #
    # This job fetches the contents of the dataset and offers them to the
    # user for download as bibliographic data.
    class ExportCitations < Base
      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["EXPORT_CITATIONS_JOB_DISABLED"] || "false").to_boolean
      end

      # Export the dataset
      #
      # @param [Task] task the task we're working from
      # @param [Hash] options parameters for this job
      # @option options [String] :format the format in which to export
      # @return [void]
      def perform(task, options)
        super
        klass = Sciveyor::Documents::Serializers::Base.for(@options[:format])

        # Make a zip file for the output
        total = dataset.document_count

        ios =
          ::Zip::OutputStream.write_buffer(StringIO.new) do |zos|
            enum = Sciveyor::Datasets::DocumentEnumerator.new(dataset: dataset)
            enum.each_with_index do |doc, i|
              zos.put_next_entry "#{doc.id.html_id}.#{@options[:format]}"
              zos.print klass.new(doc).serialize
            end
          end
        ios.rewind

        # Save it out
        task
          .uploads
          .create(
            filename: unique_filename("export_citations.zip"),
            content_type: "application/zip",
            description: "Exported Citations (ZIP)",
            short_description: "Download",
            downloadable: true
          ) { |f| Sciveyor::Storage.upload(ios, f.filename) }

        task.mark_completed
      end
    end
  end
end
