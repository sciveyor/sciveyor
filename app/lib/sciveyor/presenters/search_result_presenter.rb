# frozen_string_literal: true

module Sciveyor
  module Presenters
    # Code for formatting attributes of a SearchResult object
    class SearchResultPresenter
      include Virtus.model(strict: true, required: true)
      attribute :result, Solr::SearchResult

      # Return the path to the next page of search results
      #
      # @param [Sciveyor::Solr::Params] search_params search parameters for
      #   this request
      # @return [Sciveyor::Solr::Params] parameters to search page for
      #   the next page of results (or nil, if on the last page)
      def next_page_params(search_params)
        if result.num_hits == 0 ||
             result.solr_response["nextCursorMark"] == search_params.cursor_mark
          return nil
        end

        ret = search_params.dup
        ret.cursor_mark = result.solr_response["nextCursorMark"]
        ret
      end

      # Return a formatted version of the number of hits for the last search
      #
      # @return [String] number of hits for the search
      def num_hits_string
        num = ActiveSupport::NumberHelper.number_to_delimited(result.num_hits)

        if result.params[:q]&.!=("*:*") || result.params[:fq]&.length&.>(0)
          str = I18n.t "search.index.num_hits_found", count: result.num_hits
        else
          str =
            I18n.t "search.index.num_documents_database", count: result.num_hits
        end

        str.sub(result.num_hits.to_s, num)
      end

      # Return an array of all sort methods
      #
      # @return [Array<String>] all possible sorting strings
      def sort_methods
        SORT_METHODS.map { |m| [m, sort_string_for(m)] }
      end

      # Get the current sort method as a string
      #
      # @return [String] user-friendly representation of current sort method
      def current_sort_method
        sort_string_for result.params["sort"]
      end

      private

      # The array of all sort methods
      SORT_METHODS = [
        "score desc",
        "title asc",
        "title desc",
        "journal asc",
        "journal desc",
        "date asc",
        "date desc"
      ].freeze

      # Convert a precise sort method into a friendly string
      #
      # This function converts a sort method ('relevance', 'title', 'author',
      # 'journal', 'year') and sort direction ('asc' or 'desc') into a
      # user-friendly string.
      #
      # @param [String] method the sort method
      # @return [String] user-friendly representation of sort method
      def sort_string_for(method)
        unless SORT_METHODS.include?(method)
          return I18n.t("search.index.sort_unknown")
        end

        parts = method.split(" ")
        method = parts[0]
        dir = I18n.t("search.index.sort_#{parts[1]}")

        method_spec =
          case method
          when "score"
            I18n.t("search.index.sort_score")
          when "title"
            "#{Document.human_attribute_name("title")} #{dir}"
          when "journal"
            "#{Document.human_attribute_name("journal")} #{dir}"
          when "date"
            "#{Document.human_attribute_name("date")} #{dir}"
          end

        "#{I18n.t("search.index.sort_prefix")} #{method_spec}"
      end
    end
  end
end
