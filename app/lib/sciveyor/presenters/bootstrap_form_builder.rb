# frozen_string_literal: true

module Sciveyor
  module Presenters
    class BootstrapFormBuilder < ActionView::Helpers::FormBuilder
      def cancel(url)
        @template.link_to I18n.t("common.cancel"),
                          url,
                          class: "btn btn-secondary"
      end

      def check_group
        @template.content_tag :div, class: "mb-3" do
          yield
        end
      end

      def check_box(
        method,
        options = {},
        checked_value = "1",
        unchecked_value = "0"
      )
        label_text = options[:label]
        label_defaults = { class: "form-check-label" }
        label_options =
          merge_options(label_defaults, options.fetch(:label_options, {}))

        input_defaults = { class: "form-check-input" }
        input_options =
          merge_options(input_defaults, options.fetch(:input_options, {}))

        @template.content_tag :div, class: "form-check" do
          super(method, input_options, checked_value, unchecked_value) +
            label(method, label_text, label_options)
        end
      end

      def collection_select(
        method,
        collection,
        value_method,
        text_method,
        options = {}
      )
        select_options = options[:select_options] || {}

        standard_layout(
          method,
          options,
          "form-select"
        ) do |method, input_options|
          super(
            method,
            collection,
            value_method,
            text_method,
            select_options,
            input_options
          )
        end
      end

      def email_field(method, options = {})
        options[:client_side] = true
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      def file_field(method, options = {})
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      def number_field(method, options = {})
        options[:client_side] = true
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      def password_field(method, options = {})
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      def search_field(method, options = {})
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      def select(method, choices = nil, options = {})
        select_options = options[:select_options] || {}

        standard_layout(
          method,
          options,
          "form-select"
        ) do |method, input_options|
          super(method, choices, select_options, input_options)
        end
      end

      def submit(value = nil, options = {})
        color = options[:color] || "btn-primary"
        options[:class] = "btn #{color}"
        super(value, options)
      end

      def text_area(method, options = {})
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      def text_field(method, options = {})
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      def url_field(method, options = {})
        options[:client_side] = true
        standard_layout(method, options) do |method, input_options|
          super(method, input_options)
        end
      end

      private

      # The options[:client_side] key should be set if we're using one of the
      # special 'typed' input fields for which browsers will be doing some
      # client-side validation.
      def standard_layout(method, options = {}, input_class = "form-control")
        label_text = options[:label]
        label_defaults = { class: "form-label" }
        label_options =
          merge_options(label_defaults, options.fetch(:label_options, {}))

        input_defaults = { class: input_class }
        input_options =
          merge_options(input_defaults, options.fetch(:input_options, {}))

        error_tag = nil
        client_side_validation =
          input_options[:required] || options[:client_side]
        client_side_message = options[:client_side_message]

        if has_server_error(method) || client_side_validation
          error_tag =
            @template.content_tag(:div, class: "invalid-feedback") do
              validation_errors_for(
                method,
                client_side_validation,
                client_side_message
              )
            end
        end

        @template.content_tag :div, class: "mb-3" do
          label(method, label_text, label_options) +
            yield(method, input_options) +
            (error_tag ? error_tag : "".html_safe)
        end
      end

      def merge_options(defaults, new_options)
        (defaults.keys + new_options.keys).inject({}) do |h, key|
          h[key] = [defaults[key], new_options[key]].compact.join(" ")
          h
        end
      end

      def has_server_error(field)
        @object.present? && @object.errors[field].present?
      end

      # Return the validation errors for a field on a particular object
      #
      # These will be concatenated and separated by <br> tags, for display in the
      # invalid-feedback field.
      #
      # @param [Symbol] field the field to check for errors on
      # @param [Boolean] client_side if true, we are performing client-side
      #   validation on this field, so even if there are no server-side validation
      #   failures, we will add a 'blank' error message for this field
      # @param [String] client_side_message if set, override our default message
      #   lookup for the 'blank' error message to the given translation key
      # @return [String] the contents of the invalid-feedback div
      def validation_errors_for(
        field,
        client_side = false,
        client_side_message = nil
      )
        ret = []

        if has_server_error(field)
          server_errors =
            @template.content_tag(:span, class: "server-errors") do
              @object.errors[field]
                .map { |err| err.html_safe }
                .join(@template.tag(:br))
            end

          ret << server_errors
        end

        if client_side
          if client_side_message
            key = client_side_message
          else
            klass = @object ? @object.model_name.i18n_key : @object_name
            key = "activerecord.errors.models.#{klass}.#{field}.blank"
          end

          # If we have a server-side error message to show (on initial load), then
          # start the client-side error message as hidden, and show it when we do
          # the actual JS client-side validation.
          client_error_class = "client-errors"
          client_error_class += " d-none" if has_server_error(field)
          client_error =
            @template.content_tag(:span, class: client_error_class) do
              I18n.t(key)
            end

          ret << client_error
        end

        ret.join(@template.tag(:br)).html_safe
      end
    end
  end
end
