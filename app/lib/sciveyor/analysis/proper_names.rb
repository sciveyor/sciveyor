# frozen_string_literal: true

module Sciveyor
  # Code for performing various analyses on document text
  module Analysis
    # Compute proper name references for a given dataset
    #
    # @!attribute dataset
    #   @return [Dataset] if set, the dataset to analyze (else the entire
    #     corpus)
    class ProperNames
      include Service
      include Virtus.model(strict: true, required: false, nullify_blank: true)

      attribute :dataset, Dataset

      # Perform the proper name analysis
      #
      # @return [Hash<String, Integer>] the list of proper names extracted,
      #   with frequency counts
      def call
        total = dataset.document_count
        ret = {}

        enum = Sciveyor::Datasets::DocumentEnumerator.new(dataset: dataset)
        enum.each_with_index do |doc, i|
          lister = Documents::WordList.new
          words = lister.words_for(doc.id)

          tagged = Tagger.add_tags(words.join(" "))
          next if tagged.nil?

          nouns = Tagger.get_nouns(tagged)
          next if nouns.nil?

          ret.merge!(nouns) { |_, v1, v2| v1 + v2 }
        end

        ret.sort_by { |(_, v)| -v }
      end
    end
  end
end
