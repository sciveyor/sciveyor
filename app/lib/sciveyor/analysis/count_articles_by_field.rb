# frozen_string_literal: true

module Sciveyor
  module Analysis
    # Code for counting the number of articles in a dataset, grouped by a field
    #
    # @!attribute field
    #   @return [Symbol] the field to group by
    # @!attribute dataset
    #   @return [Dataset] if set, the dataset to analyze (else the entire
    #     corpus)
    # @!attribute normalize
    #   @return [Boolean] if true, divide the counts for `dataset` by the
    #     counts for the same field in `normalization_dataset` before returning
    # @!attribute normalization_dataset
    #   @return [Dataset] dataset to normalize by (or nil for the whole corpus)
    class CountArticlesByField
      include Service
      include Virtus.model(strict: true, required: false, nullify_blank: true)

      attribute :field, Symbol
      attribute :dataset, Dataset
      attribute :normalize, Boolean, default: false
      attribute :normalization_dataset, VirtusExt::DatasetId

      # Count up a dataset (or the corpus) by a field
      #
      # This function takes a Solr field, groups the articles of interest by
      # the values for the given field, and returns the result in a hash.
      #
      # @todo This function should support the same kind of work with names
      #   that we have in Sciveyor::Documents::Author.
      #
      # @return [Result] results of analysis
      def call
        Result.new(
          counts: normalize_counts(dataset ? group_dataset : group_corpus),
          normalize: normalize,
          normalization_dataset: normalization_dataset
        )
      end

      private

      # Walk a dataset manually and group it by field
      #
      # @return [Hash<String, Integer>] number of documents in each group
      def group_dataset
        ret = {}
        total = dataset.document_count

        enum = Sciveyor::Datasets::DocumentEnumerator.new(dataset: dataset)
        enum.each_with_index do |doc, i|
          key = get_field_from_document(doc)
          next if key.nil?

          ret[key] ||= 0
          ret[key] += 1
        end

        ret
      end

      # Group the entire corpus by field, using Solr's result grouping
      #
      # @return [Hash<String, Integer>] number of documents in each group
      def group_corpus
        ret = {}
        start = 0

        num_docs = 0
        total_docs = Sciveyor::Solr::CorpusStats.new.size

        loop do
          search_result =
            Sciveyor::Solr::Connection.search_raw(
              :q => "*:*",
              :group => "true",
              "group.field" => field.to_s,
              :fl => "id",
              :facet => "false",
              :start => start.to_s,
              :rows => 100
            )

          # These conditions would indicate a malformed Solr response
          break unless search_result.dig("grouped", field.to_s, "matches")

          grouped = search_result["grouped"][field.to_s]
          break if grouped["matches"].zero?

          groups = grouped["groups"]
          break unless groups

          # This indicates that we're out of records
          break if groups.empty?

          # Add this batch to the return
          groups.each do |g|
            key = g["groupValue"]
            next if key.nil?

            # Group by years if we're asked to group by date
            key = DateTime.iso8601(key).year.to_s if field == :date

            val = g["doclist"]["numFound"]

            ret[key] ||= 0
            ret[key] += val
          end

          # Get the next batch of groups
          start += 100
        end

        ret
      end

      # Get the value of the field for grouping from a document
      #
      # This implements support for grouping on journals and years.
      #
      # @param [Document] doc the Solr document
      # @return [String] the field value
      def get_field_from_document(doc)
        return doc.journal if field == :journal
        return doc.date.year.to_s if field == :date

        # We're not faceting on other fields, and this won't work at all
        # yet with author grouping. (FIXME?)
        # :nocov:
        nil
        # :nocov:
      end

      # Fill in zeros for any missing values in the counts
      #
      # If field is year, we'll actually fill in the intervening years by
      # count. Otherwise, we'll just fill in any values that are present in
      # the normalization set but missing in the counts.
      #
      # @param [Hash<String, Numeric>] counts the counts queried
      # @param [Hash<String, Numeric>] normalization_counts the counts from the
      #   normalization set, nil if we didn't normalize
      # @return [Hash<String, Numeric>] the counts with intervening values set
      #   to zero
      def zero_intervening(counts, normalization_counts = nil)
        normalization_counts ||= {}

        if field == :date
          # Find the low and high values for the numerical contents here, if
          # asked to do so
          min = 99_999
          max = -99_999

          full_set = (counts.keys + normalization_counts.keys).compact
          full_set.each do |k|
            num = 0
            begin
              num = Integer(k)
            rescue ArgumentError
              next
            end

            min = num if num < min
            max = num if num > max
          end

          # Fill in all of the numerically intervening years
          Range.new(min, max).each { |i| counts[i.to_s] ||= 0.0 }
        else
          normalization_counts.keys.compact.each { |k| counts[k] ||= 0.0 }
        end

        counts
      end

      # Normalize the counts against the normalization set, if it exists
      #
      # This function also makes sure that the field values are a contiguous
      # range with no gaps, filling in zeros if necessary.
      #
      # @param [Hash<String, Integer>] counts the counts for the original set
      # @param [Hash<String, Float>] the normalized counts
      def normalize_counts(counts)
        return {} if counts.empty?
        return zero_intervening(counts) unless normalize

        norm_counts =
          CountArticlesByField.call(
            field: field,
            dataset: normalization_dataset
          ).counts

        ret =
          counts.each_with_object({}) do |(k, v), out|
            # Just don't save an entry for any really strange values here
            if k.present? && norm_counts[k]&.>(0)
              out[k] = v.to_f / norm_counts[k]
            end
          end

        zero_intervening(ret, norm_counts)
      end
    end
  end
end
