# frozen_string_literal: true

# Supplemental actions for users
#
# We add a number of actions for users that aren't part of the standard Devise
# suite.
class UsersController < ApplicationController
  before_action :authenticate_user!

  # Start a job to build an export
  #
  # @return [void]
  def export_create
    unless current_user.can_export?
      raise ArgumentError, "user has exported too recently"
    end

    task =
      Task.create(name: "UserExport", type: "UserExport", user: current_user)
    task.job_start

    current_user.export_requested_at = Time.current
    current_user.save

    redirect_to edit_user_registration_path
  end

  # Remove the user's export file, if it exists
  #
  # @return [void]
  def export_delete
    raise ActiveRecord::RecordNotFound unless current_user.export_filename

    Sciveyor::Storage.delete(current_user.export_filename)

    current_user.export_filename = nil
    current_user.export_content_type = nil
    current_user.save

    redirect_to edit_user_registration_path
  end
end
