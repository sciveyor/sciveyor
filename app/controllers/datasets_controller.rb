# frozen_string_literal: true

# Display, modify, delete, and analyze datasets belonging to a given user
#
# This controller is responsible for the handling of the datasets which
# belong to a given user.  It displays the user's list of datasets, and
# handles the starting and management of the user's background analysis
# tasks.
#
# @see Dataset
class DatasetsController < ApplicationController
  before_action :authenticate_user!

  # Show all of the current user's saved datasets
  #
  # @return [void]
  def index
    @datasets = current_user.datasets_hash

    render
  end

  # Show the dataset table for fast updates
  #
  # @return [void]
  def load
    disable_browser_cache

    @datasets = current_user.datasets_hash
    render layout: false
  end

  # Show information about the requested dataset
  #
  # @return [void]
  def show
    @dataset = Dataset.find_by!(uuid: params[:id])
  end

  # Show the form for creating a new dataset
  #
  # @return [void]
  def new
    render layout: false
  end

  # Create a new dataset in the database
  #
  # If a dataset with these queries already exists, the dataset will be added
  # to the user's list of datasets of interest.
  #
  # @return [void]
  def create
    attributes = {
      q: params[:q] || [],
      boolean: params[:boolean] || "and",
      fq: params[:fq] || []
    }

    # There is a very strange Solr bug whereby if the overall query string
    # starts with an open curly brace, an entirely different query parser is
    # activated. We add those spaces, but that whitespace is stripped by the
    # Rails parameters system. Put them back right here.
    attributes[:q].map! do |q|
      q.prepend(" ") if q.start_with?("{")
      q
    end

    dataset = Dataset.find_or_create_by(attributes)
    current_user.add_dataset(dataset, params[:name])

    Rails.logger.info(
      short_message: "Created new dataset",
      _method: "datasets#create",
      _user_id: current_user.id,
      _dataset_uuid: dataset.uuid,
      _q: attributes[:q],
      _fq: attributes[:fq],
      _boolean: attributes[:boolean],
      _is_workflow: current_user.workflow_active
    )

    if current_user.workflow_active
      current_user.workflow_datasets << dataset.uuid
      current_user.save

      redirect_to workflow_activate_path(current_user.workflow_class),
                  flash: {
                    success: I18n.t("datasets.create.workflow")
                  }
    else
      redirect_to datasets_path,
                  flash: {
                    success: I18n.t("datasets.create.success")
                  }
    end
  end

  # Remove a dataset from the user's list of saved datasets
  #
  # @return [void]
  def destroy
    @dataset = Dataset.find_by!(uuid: params[:id])
    current_user.remove_dataset(@dataset)

    Rails.logger.info(
      short_message: "Destroyed dataset",
      _method: "datasets#destroy",
      _user_id: current_user.id,
      _dataset_uuid: @dataset.uuid
    )

    redirect_to datasets_path
  end

  # Add a single document to a dataset
  #
  # This is an odd update method.  The only attribute that we allow you to send
  # in here is the ID of a single document to be added into the dataset.
  # Any other attempts to PATCH dataset attributes will be silently ignored.
  #
  # @return [void]
  def update
    raise ActionController::ParameterMissing, :doc_id unless params[:doc_id]

    @dataset = Dataset.find_by!(uuid: params[:id])
    @document = Document.find(params[:doc_id])

    # You can't do this to an 'and' dataset, unless it has only one query and
    # so we can change it to 'or'
    if @dataset.boolean.nil? || @dataset.boolean == "and"
      if @dataset.q.length > 1
        redirect_to dataset_path(@dataset),
                    alert: I18n.t("datasets.update.boolean_error")

        return
      end
    end

    # See if we can find an 'id' query to alter
    edited = false
    boolean = @dataset.boolean

    queries =
      @dataset.q.map do |query|
        if query.start_with?("id:")
          edited = true
          query.delete_suffix(")") + " OR \"#{params[:doc_id]}\")"
        else
          query
        end
      end

    # Add a new 'id' query, change the boolean
    unless edited
      boolean = "or"
      queries << "id:(\"#{params[:doc_id]}\")"
    end

    # Get the dataset
    new_dataset =
      Dataset.find_or_create_by(q: queries, boolean: boolean, fq: @dataset.fq)

    # Swap the name pointer to point to the new dataset
    name = @dataset.name_for(current_user)
    current_user.remove_dataset(@dataset)
    current_user.add_dataset(new_dataset, name)

    Rails.logger.info(
      short_message: "Added document to dataset",
      _method: "datasets#update",
      _user_id: current_user.id,
      _new_dataset_uuid: new_dataset.uuid,
      _new_dataset_q: new_dataset.q,
      _old_dataset_uuid: @dataset.uuid,
      _old_dataset_q: @dataset.q
    )

    redirect_to dataset_path(new_dataset)
  end
end
