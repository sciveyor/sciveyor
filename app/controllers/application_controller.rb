# frozen_string_literal: true

# The main application controller for Sciveyor
#
# This controller implements functionality shared throughout the entire
# Sciveyor site.
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout :layout_for

  # Render the full_page layout on Devise views
  #
  # @return [String] the layout to render
  def layout_for
    devise_controller? ? "full_page" : "application"
  end

  private

  before_action :set_locale, :set_timezone
  before_action :configure_permitted_parameters, if: :devise_controller?

  # Set the locale if the user is logged in
  #
  # This function is called as a `before_action` in all controllers, you do
  # not need to call it yourself.  Do not disable it, or the locale system
  # will go haywire.
  #
  # @return [void]
  def set_locale
    I18n.locale =
      user_signed_in? ? current_user.language.to_sym : I18n.default_locale
  end

  # Set the timezone if the user is logged in
  #
  # This function is called as a `before_action` in all controllers, you do
  # not need to call it yourself.  Do not disable it, or the timezone system
  # will go haywire.
  #
  # @return [void]
  def set_timezone
    Time.zone = user_signed_in? ? current_user.timezone : "America/New_York"
  end

  protected

  # Redirect to the root on successful sign in
  #
  # This method is called by Devise.
  #
  # @param [User] resource the user that just signed in
  # @return [void]
  def after_sign_in_path_for(_resource)
    root_url
  end

  # Redirect to the root on successful sign out
  #
  # This method is called by Devise.
  #
  # @return [void]
  def after_sign_out_path_for(_resource)
    root_url
  end

  # Set cache control headers
  #
  # This helper can be called when we want a page to expire.  This is similar
  # to Rails' `expires_now` function, but sets more headers to work with more
  # browsers.
  #
  # @return [void]
  def disable_browser_cache
    response.cache_control[:no_cache] = true
    response.cache_control[:extras] = %w[no-store max-age=0 must-revalidate]
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  # Get the path to an error template
  #
  # This checks for a localized error page, just like the internal Rails code
  # that renders 404/500s.
  #
  # @param [String] error error code to look for
  # @return [String] file path to error page
  def error_page_path(error = "404")
    path = Rails.root.join("public", "#{error}.#{I18n.locale}.html")
    return path if File.exist?(path)

    Rails.root.join("public", "#{error}.html")
  end

  # Send the right parameter sanitizers to Devise
  #
  # Attributes that can be edited by the user (in the user options form)
  # should be whitelisted here.  This should be kept in sync with the views
  # in users/registrations/{ edit, new }.html.
  #
  # This method is not tested, as it's only ever called from within the
  # internals of Devise.
  #
  # @return [void]
  # :nocov:
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(
      :sign_up,
      keys: %i[name email password password_confirmation language timezone]
    )
    devise_parameter_sanitizer.permit(
      :account_update,
      keys: %i[
        name
        email
        password
        password_confirmation
        current_password
        language
        timezone
      ]
    )
  end
  # :nocov:
end
