# frozen_string_literal: true

# Search and browse the document database
#
# This controller displays both traditional and advanced search pages and the
# resulting lists of documents.
#
# @see Sciveyor::Solr::Connection.search
class SearchController < ApplicationController
  # Show the main search index page
  #
  # @return [void]
  def index
    set_params
    render
  end

  # Show only the results table (for infinite scroll)
  #
  # @return [void]
  def load
    set_params
    render layout: false
  end

  # Show the advanced search page
  #
  # @return [void]
  def advanced
  end

  private

  # Set the parameters for running a search
  #
  # @return [void]
  def set_params
    @search_params =
      Sciveyor::Solr::Params.new(params: params, api: request.format != :html)

    @result = Sciveyor::Solr::Connection.search(@search_params.to_solr)
    @result_presenter =
      Sciveyor::Presenters::SearchResultPresenter.new(result: @result)
  end
end
