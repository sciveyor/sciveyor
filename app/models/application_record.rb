# frozen_string_literal: true

# Base class from which all our models inherit
class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
end
