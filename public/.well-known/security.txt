-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Contact: mailto:charles@charlespence.net
Expires: 2024-01-01T00:00:00.000Z
Encryption: https://keyoxide.org/F371AEEAF417E9490447C9C61F6099E4BD9DEC90
Acknowledgments: https://docs.sciveyor.com/docs/thanks/
Preferred-Languages: en,fr
Canonical: https://www.sciveyor.com/.well-known/security.txt
Policy: https://docs.sciveyor.com/docs/security/
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEE83Gu6vQX6UkER8nGH2CZ5L2d7JAFAmKHO8EACgkQH2CZ5L2d
7JBy+RAAgFNvMfwQLLX0UeFh/RmFjxUcQ5TCo0+6oUL5xfQU78bDBM5I8d03KB+b
gr5+/ZW0WzJnJApEHahUuFGBmZevIQcrngtjQ00Yys2tqHF+GzKy5RsrfVWEPIDb
8MAWTUzUuCkpP7D0e9q+eIgboqluGPNuNSXPCY7XGagijVrcv+noni5NVzu247Eu
jNdSnUDkRR+RQ1xEygj4Bl1wh1Bim82c6+SH0Jtnj+Hf1rml1mzoHX//OQCLEo2+
WPydw2DLSxmaST1NwGGRwq8S6DK0GwKG0QYBF6K18CDoAMHhLueEHepdCw1Tb947
vScItB/VyNnjqPhRudVflMvhr1YTFh6+Zm9hyuQQB8mdB+S22LIiPz7D5mJTFz4g
rovDWHPBzUjNlz083ow4SeAy0iZtpdQUgSmsWIdYBdfvPBcoTHjWXBNCJ/M4dN5m
MPcDp4+KWdbbd0mwjeZKD7s6T5SWSASSqn+6Rm2gUnYfP9COKbhUM+eLuCxi9XPH
InTbNc3F7WXj1LMGqBCy7Hmsa4B12D58j80KPesgDJfv2ng+RYbpWnrGjiUemC/a
osBN09SyoX/bFre6+ofC28wzqDN9SJRN1iOQsXxrz//1213oJiHEFUfQRrbNKX4W
DCO563Dztb/SwMsA7wPbMV3X2oQfXhTC7kqitTi5BpnLA1A8cH4=
=1/Ug
-----END PGP SIGNATURE-----
