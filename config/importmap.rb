# Pin npm packages by running ./bin/importmap

# Entrypoint
pin "application", preload: true

# Local separate modules
pin "jobs/all_toggle"
pin "jobs/word_cloud"
pin "jobs/article_dates/params"
pin "jobs/article_dates/results"
pin "jobs/craig_zeta/results"
pin "jobs/network/results"
pin "jobs/term_dates/results"
pin "jobs/word_frequency/params"
pin "search/advanced"
pin "search/index"
pin "users/edit"
pin "util/auto_fetch"
pin "util/cookies"
pin "util/from_html"
pin "util/global"
pin "util/onready"
pin "util/show_hide"
pin "util/shuffle"

# Vendored and manually managed JS
pin "bootstrap"
pin "fa-solid"
pin "fontawesome"

# External JS
pin "@popperjs/core", to: "https://ga.jspm.io/npm:@popperjs/core@2.11.5/dist/esm/index.js"
pin "@rails/ujs", to: "https://ga.jspm.io/npm:@rails/ujs@6.0.5/lib/assets/compiled/rails-ujs.js"
pin "d3-array", to: "https://ga.jspm.io/npm:d3-array@3.1.6/src/index.js"
pin "d3-cloud", to: "https://ga.jspm.io/npm:d3-cloud@1.2.5/build/d3.layout.cloud.js"
pin "d3-color", to: "https://ga.jspm.io/npm:d3-color@3.1.0/src/index.js"
pin "d3-dispatch", to: "https://ga.jspm.io/npm:d3-dispatch@3.0.1/src/index.js"
pin "d3-ease", to: "https://ga.jspm.io/npm:d3-ease@3.0.1/src/index.js"
pin "d3-force", to: "https://ga.jspm.io/npm:d3-force@3.0.0/src/index.js"
pin "d3-format", to: "https://ga.jspm.io/npm:d3-format@3.1.0/src/index.js"
pin "d3-interpolate", to: "https://ga.jspm.io/npm:d3-interpolate@3.0.1/src/index.js"
pin "d3-quadtree", to: "https://ga.jspm.io/npm:d3-quadtree@3.0.1/src/index.js"
pin "d3-scale", to: "https://ga.jspm.io/npm:d3-scale@4.0.2/src/index.js"
pin "d3-scale-chromatic", to: "https://ga.jspm.io/npm:d3-scale-chromatic@3.0.0/src/index.js"
pin "d3-selection", to: "https://ga.jspm.io/npm:d3-selection@3.0.0/src/index.js"
pin "d3-time", to: "https://ga.jspm.io/npm:d3-time@3.0.0/src/index.js"
pin "d3-time-format", to: "https://ga.jspm.io/npm:d3-time-format@4.1.0/src/index.js"
pin "d3-timer", to: "https://ga.jspm.io/npm:d3-timer@3.0.1/src/index.js"
pin "d3-transition", to: "https://ga.jspm.io/npm:d3-transition@3.0.1/src/index.js"
pin "ev-emitter", to: "https://ga.jspm.io/npm:ev-emitter@2.1.2/ev-emitter.js"
pin "fizzy-ui-utils", to: "https://ga.jspm.io/npm:fizzy-ui-utils@3.0.0/utils.js"
pin "infinite-scroll", to: "https://ga.jspm.io/npm:infinite-scroll@4.0.1/js/index.js"
pin "internmap", to: "https://ga.jspm.io/npm:internmap@2.0.3/src/index.js"
