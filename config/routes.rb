# frozen_string_literal: true

Rails.application.routes.draw do
  # The user workflow
  get "workflow" => "workflow#index"
  get "workflow/start" => "workflow#start"
  get "workflow/stop" => "workflow#stop"
  get "workflow/info/:class" => "workflow#info",
      :as => "workflow_info",
      :constraints => {
        class: /[A-Z][A-Za-z]+/
      }
  get "workflow/activate/:class" => "workflow#activate",
      :as => "workflow_activate",
      :constraints => {
        class: /[A-Z][A-Za-z]+/
      }
  get "workflow/options/:class" => "workflow#options",
      :as => "workflow_options",
      :constrants => {
        class: /[A-Z][A-Za-z]+/
      }
  post "workflow/run/:class" => "workflow#run",
       :as => "workflow_run",
       :constraints => {
         class: /[A-Z][A-Za-z]+/
       }
  get "workflow/fetch" => "workflow#fetch"
  get "workflow/fetch_load" => "workflow#fetch_load"
  delete "workflow/:id" => "workflow#destroy", :as => :destroy_task
  get "workflow/:id/view/:template" => "workflow#view", :as => :view_task

  # Start off on the landing/dashboard page
  root to: "workflow#index"

  # Search/Browse page
  get "search" => "search#index"
  get "search/load" => "search#load"
  get "search/advanced" => "search#advanced"

  # Documents
  get "documents/:id/export" => "documents#export",
      :as => "documents_export",
      :constraints => {
        id: /.*/
      }

  # Datasets
  resources :datasets, except: :edit do
    collection { get "load" }
  end

  # User login routes
  devise_for :users, skip: [:sessions], controllers: { passwords: "passwords" }
  devise_scope :user do
    # We only want users to sign in using the dropdown box on the main page,
    # not by visiting /users/sign_in, so we don't create a get 'sign_in' route
    # here.
    post "users/sign_in" => "devise/sessions#create", :as => :user_session
    match "users/sign_out" => "devise/sessions#destroy",
          :as => :destroy_user_session,
          :via => Devise.mappings[:user].sign_out_via

    # User export manipulation
    get "users/export" => "users#export_create", :as => :user_export
    delete "users/export" => "users#export_delete", :as => ""

    # Redirect to the root after a successful user edit
    get "users" => "workflow#index"
  end

  scope "/users" do
    # Libraries
    resources :libraries, except: :show do
      collection { get "query" }
    end
  end

  # Static content
  get "static/cookies" => "static#cookies"
  post "static/echo" => "static#echo"
  get "static/user_data" => "static#user_data"

  # Static controller also is rendering files and images stored remotely
  get "file/*name" => "static#file", :format => false, :as => "static_file"
  get "manifest" => "static#manifest",
      :format => true,
      :constraints => {
        format: :json
      }
  get "browserconfig" => "static#browserconfig",
      :format => true,
      :constraints => {
        format: :xml
      }

  # unAPI service
  get "unapi" => "unapi#index"

  #
  # API v1
  #
  scope "api/v1", constraints: { format: :json } do
    get "lists/authors" => "lists#authors"
    get "lists/journals" => "lists#journals"
  end
end
