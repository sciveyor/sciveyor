# frozen_string_literal: true

namespace :sciveyor do
  namespace :maintenance do
    desc "Remove old finished tasks from the database"
    task expire_tasks: :environment do
      num = Task.where("created_at < ?", 2.weeks.ago).destroy_all.count

      Rails.loger.info(
        short_message: "Removed old finished tasks from the database",
        _method: "expire_tasks",
        _count: num
      )
    end
  end
end
