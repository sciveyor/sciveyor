#!/bin/bash
# Run the web server (Docker entrypoint)

# If Doppler is configured, use it
if [[ ! -z "$DOPPLER_TOKEN" ]]; then
  doppler run -- bundle exec puma
else
  bundle exec puma
fi
