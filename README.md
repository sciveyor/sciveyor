<p align="center">
<img src="https://docs.sciveyor.com/images/icon-orange.svg" width="20%"
height="auto" alt="Sciveyor Logo">
</p>

# Sciveyor

[![Status: Active](./docs/badges/status-active.svg)](#)
[![Version: v4.0.0-alpha](./docs/badges/version.svg)](https://codeberg.org/sciveyor/sciveyor/releases)
[![License: MIT](./docs/badges/license-mit.svg)](#copyright)
[![Production Site](./docs/badges/production.svg)](https://www.sciveyor.com/)
[![Issue Tracker](./docs/badges/issue-tracker.svg)](https://codeberg.org/sciveyor/sciveyor/issues)
[![Docs: Portal](./docs/badges/docs-portal.svg)](https://docs.sciveyor.com/)
[![Docs: User Manual](./docs/badges/docs-manual.svg)](https://docs.sciveyor.com/manual/)
[![Docs: API Reference](./docs/badges/docs-api.svg)](https://docs.sciveyor.com/docs/api/)

(_Note:_ This README file describes the unreleased Git version, not the last
release, which may be different. Select the appropriate release tag at the top
left to see the README for the version you're using.)

Sciveyor is an application designed to let users perform complex searches as
well as digital-humanities and text-mining analysis tasks on a corpus of journal
articles.

## Documentation Portal

For information about Sciveyor's features, please consult the user manual, which
you can find at [our documentation portal online.](https://docs.sciveyor.com)

## Contributors / Support

Thanks to everyone who has contributed code, translations, and ideas to
Sciveyor! For a full list of our thanks and acknowledgments over the long
development history of Sciveyor, please visit
[our thanks page on the developer documentation portal.](https://docs.sciveyor.com/docs/thanks/)
Our fantastic logo was designed by
[Miles Bensky.](https://www.instagram.com/mbenskydesigns/)

Charles Pence was supported in the development of Sciveyor by the
[Fonds de la Recherche Scientifique - FNRS](https://www.frs-fnrs.be/) under
grant no. F.4526.19. For the previous versions of Sciveyor (evoText and
RLetters), Charles Pence and Grant Ramsey were supported by the
[National Science Foundation](http://www.nsf.gov), HPS Scholars Award grant no.
1456573, and the
[National Evolutionary Synthesis Center (NESCent),](http://www.nescent.org) NSF
grant no. EF-0905606.

[![FNRS][fnrs_img]][fnrs] [![National Science Foundation][nsf_img]][nsf]
[![National Evolutionary Synthesis Ceter][nescent_img]][nescent]

[fnrs]: https://frs-fnrs.be
[fnrs_img]: ./docs/badges/fnrs.png
[nsf]: https://nsf.gov
[nsf_img]: ./docs/badges/nsf.gif
[nescent]: https://nescent.org
[nescent_img]: ./docs/badges/nescent.png

## Copyright

Sciveyor &copy; 2011–2023 [Charles Pence](mailto:charles@charlespence.net).
Sciveyor is licensed under the MIT license. Please see the [COPYING](COPYING)
document for more information.

The stop lists found in `app/lib/sciveyor/analysis/stop_list` are released under
the BSD license by the Apache Solr project. The colors in
`app/lib/sciveyor/visualization/color_brewer` are released by
[the ColorBrewer project](http://www.colorbrewer.org) under the Apache license.
