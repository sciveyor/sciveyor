# frozen_string_literal: true

FactoryBot.define do
  factory :document, class: Document do
    transient do
      schema { "https://data.sciveyor.com/schema" }
      version { 5 }
      id { "doi:10.1234/this.is.a.doi" }
      doi { nil }
      externalIds { nil }
      license { nil }
      licenseUrl { nil }
      dataSource { nil }
      dataSourceUrl { nil }
      dataSourceVersion { nil }
      type { "article" }
      title { nil }
      authors { nil }
      date { nil }
      dateElectronic { nil }
      dateAccepted { nil }
      dateReceived { nil }
      journal { nil }
      volume { nil }
      number { nil }
      pages { nil }
      keywords { nil }
      tags { nil }
      term_vectors { nil }
    end

    factory :full_document do
      authors do
        [
          Sciveyor::Documents::Author.new(
            affiliation:
              "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China",
            first: "Pei-Wei",
            last: "Chiu",
            name: "Pei-Wei Chiu"
          ),
          Sciveyor::Documents::Author.new(
            affiliation:
              "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China",
            first: "Yu-Chang",
            last: "Huang",
            name: "Yu-Chang Huang"
          ),
          Sciveyor::Documents::Author.new(
            affiliation:
              "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China",
            first: "Yu-Jiao",
            last: "Pan",
            name: "Yu-Jiao Pan"
          ),
          Sciveyor::Documents::Author.new(
            affiliation:
              "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China",
            first: "Chih-Hung",
            last: "Wang",
            name: "Chih-Hung Wang"
          ),
          Sciveyor::Documents::Author.new(
            affiliation:
              "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China",
            first: "Chin-Hung",
            last: "Sun",
            name: "Chin-Hung Sun"
          )
        ]
      end
      dataSource { "Public Library of Science" }
      dataSourceUrl { "https://data.sciveyor.com/source/plos" }
      dataSourceVersion { 1 }
      date { DateTime.new(2010, 5, 1) }
      dateAccepted { DateTime.new(2010, 3, 23) }
      dateReceived { DateTime.new(2009, 7, 31) }
      doi { "10.1371/journal.pntd.0000677" }
      externalIds { ["pii:09-PNTD-RA-0392R3"] }
      id { "doi:10.1371/journal.pntd.0000677" }
      journal { "PLoS Neglected Tropical Diseases" }
      license do
        "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited."
      end
      licenseUrl { "https://plos.org/terms-of-use/" }
      number { "5" }
      pages { "e677" }
      schema { "https://data.sciveyor.com/schema" }
      tags do
        [
          "Biochemistry/Cell Signaling and Trafficking Structures",
          "Cell Biology/Extra-Cellular Matrix",
          "Developmental Biology/Developmental Evolution",
          "Developmental Biology/Microbial Growth and Development",
          "Evolutionary Biology/Microbial Evolution and Genomics",
          "Infectious Diseases/Neglected Tropical Diseases",
          "Infectious Diseases/Protozoal Infections",
          "Infectious Diseases/Tropical and Travel-Associated Diseases"
        ]
      end
      title do
        "A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia"
      end
      type { "article" }
      version { 5 }
      volume { "4" }

      term_vectors do
        {
          "1" => {
            "tf" => 2,
            "positions" => [12, 456],
            "df" => 1341.0
          },
          "12" => {
            "tf" => 1,
            "positions" => [274],
            "df" => 798.0
          },
          "13" => {
            "tf" => 2,
            "positions" => [275, 359],
            "df" => 690.0
          },
          "14" => {
            "tf" => 1,
            "positions" => [340],
            "df" => 601.0
          },
          "15" => {
            "tf" => 1,
            "positions" => [358],
            "df" => 625.0
          },
          "16" => {
            "tf" => 4,
            "positions" => [388, 437, 446, 460],
            "df" => 461.0
          },
          "17" => {
            "tf" => 2,
            "positions" => [400, 425],
            "df" => 467.0
          },
          "2" => {
            "tf" => 5,
            "positions" => [13, 54, 83, 113, 387],
            "df" => 1174.0
          },
          "3" => {
            "tf" => 1,
            "positions" => [26],
            "df" => 1073.0
          },
          "4" => {
            "tf" => 1,
            "positions" => [144],
            "df" => 1039.0
          },
          "5" => {
            "tf" => 1,
            "positions" => [145],
            "df" => 1000.0
          },
          "6" => {
            "tf" => 2,
            "positions" => [155, 181],
            "df" => 940.0
          },
          "711" => {
            "tf" => 1,
            "positions" => [225],
            "df" => 19.0
          },
          "79" => {
            "tf" => 3,
            "positions" => [255, 295, 499],
            "df" => 50.0
          },
          "9" => {
            "tf" => 2,
            "positions" => [315, 339],
            "df" => 794.0
          },
          "acid" => {
            "tf" => 1,
            "positions" => [137],
            "df" => 88.0
          },
          "acidic" => {
            "tf" => 1,
            "positions" => [454],
            "df" => 11.0
          },
          "adheres" => {
            "tf" => 1,
            "positions" => [37],
            "df" => 4.0
          },
          "after" => {
            "tf" => 1,
            "positions" => [111],
            "df" => 500.0
          },
          "all" => {
            "tf" => 1,
            "positions" => [276],
            "df" => 640.0
          },
          "andor" => {
            "tf" => 1,
            "positions" => [234],
            "df" => 212.0
          },
          "annotated" => {
            "tf" => 1,
            "positions" => [441],
            "df" => 8.0
          },
          "any" => {
            "tf" => 1,
            "positions" => [301],
            "df" => 230.0
          },
          "architectural" => {
            "tf" => 1,
            "positions" => [193],
            "df" => 1.0
          },
          "arises" => {
            "tf" => 1,
            "positions" => [87],
            "df" => 12.0
          },
          "been" => {
            "tf" => 2,
            "positions" => [259, 434],
            "df" => 1235.0
          },
          "before" => {
            "tf" => 1,
            "positions" => [249],
            "df" => 158.0
          },
          "between" => {
            "tf" => 2,
            "positions" => [323, 331],
            "df" => 655.0
          },
          "biological" => {
            "tf" => 1,
            "positions" => [196],
            "df" => 126.0
          },
          "biosynthetic" => {
            "tf" => 1,
            "positions" => [214],
            "df" => 7.0
          },
          "bonds" => {
            "tf" => 2,
            "positions" => [322, 353],
            "df" => 2.0
          },
          "both" => {
            "tf" => 1,
            "positions" => [72],
            "df" => 607.0
          },
          "capable" => {
            "tf" => 1,
            "positions" => [105],
            "df" => 61.0
          },
          "carried" => {
            "tf" => 1,
            "positions" => [59],
            "df" => 140.0
          },
          "causes" => {
            "tf" => 1,
            "positions" => [17],
            "df" => 241.0
          },
          "causing" => {
            "tf" => 1,
            "positions" => [8],
            "df" => 158.0
          },
          "cells" => {
            "tf" => 3,
            "positions" => [25, 273, 345],
            "df" => 284.0
          },
          "composed" => {
            "tf" => 2,
            "positions" => [119, 150],
            "df" => 37.0
          },
          "concentrated" => {
            "tf" => 1,
            "positions" => [241],
            "df" => 24.0
          },
          "conserved" => {
            "tf" => 1,
            "positions" => [293],
            "df" => 66.0
          },
          "contaminated" => {
            "tf" => 1,
            "positions" => [94],
            "df" => 73.0
          },
          "covered" => {
            "tf" => 1,
            "positions" => [364],
            "df" => 25.0
          },
          "cwp13" => {
            "tf" => 1,
            "positions" => [201],
            "df" => 1.0
          },
          "cwp3" => {
            "tf" => 1,
            "positions" => [306],
            "df" => 1.0
          },
          "cwps" => {
            "tf" => 7,
            "positions" => [188, 239, 278, 332, 391, 473, 491],
            "df" => 2.0
          },
          "cx2c" => {
            "tf" => 1,
            "positions" => [484],
            "df" => 1.0
          },
          "cxc" => {
            "tf" => 2,
            "positions" => [464, 482],
            "df" => 1.0
          },
          "cyst" => {
            "tf" => 11,
            "positions" => [67, 79, 99, 179, 185, 211, 253, 313, 337, 356, 430],
            "df" => 32.0
          },
          "cysteine" => {
            "tf" => 2,
            "positions" => [428, 452],
            "df" => 23.0
          },
          "cysteinerich" => {
            "tf" => 1,
            "positions" => [372],
            "df" => 6.0
          },
          "cysteines" => {
            "tf" => 2,
            "positions" => [294, 325],
            "df" => 1.0
          },
          "cysts" => {
            "tf" => 1,
            "positions" => [89],
            "df" => 44.0
          },
          "deletion" => {
            "tf" => 1,
            "positions" => [296],
            "df" => 17.0
          },
          "diarrhea" => {
            "tf" => 1,
            "positions" => [20],
            "df" => 38.0
          },
          "diarrheal" => {
            "tf" => 1,
            "positions" => [10],
            "df" => 15.0
          },
          "different" => {
            "tf" => 1,
            "positions" => [393],
            "df" => 471.0
          },
          "digestion" => {
            "tf" => 1,
            "positions" => [384],
            "df" => 14.0
          },
          "disease" => {
            "tf" => 1,
            "positions" => [11],
            "df" => 1125.0
          },
          "disk" => {
            "tf" => 1,
            "positions" => [41],
            "df" => 2.0
          },
          "disulfide" => {
            "tf" => 2,
            "positions" => [321, 352],
            "df" => 3.0
          },
          "divide" => {
            "tf" => 1,
            "positions" => [74],
            "df" => 5.0
          },
          "does" => {
            "tf" => 1,
            "positions" => [494],
            "df" => 103.0
          },
          "downstream" => {
            "tf" => 1,
            "positions" => [60],
            "df" => 14.0
          },
          "dtt" => {
            "tf" => 1,
            "positions" => [347],
            "df" => 3.0
          },
          "during" => {
            "tf" => 4,
            "positions" => [138, 223, 237, 403],
            "df" => 542.0
          },
          "encoding" => {
            "tf" => 1,
            "positions" => [206],
            "df" => 48.0
          },
          "encystation" => {
            "tf" => 5,
            "positions" => [65, 224, 238, 245, 407],
            "df" => 4.0
          },
          "encysting" => {
            "tf" => 1,
            "positions" => [344],
            "df" => 2.0
          },
          "endoplasmic" => {
            "tf" => 1,
            "positions" => [421],
            "df" => 11.0
          },
          "environments" => {
            "tf" => 1,
            "positions" => [110],
            "df" => 62.0
          },
          "enzyme" => {
            "tf" => 2,
            "positions" => [208, 383],
            "df" => 56.0
          },
          "epithelial" => {
            "tf" => 1,
            "positions" => [24],
            "df" => 34.0
          },
          "equivalents" => {
            "tf" => 1,
            "positions" => [268],
            "df" => 3.0
          },
          "er" => {
            "tf" => 1,
            "positions" => [423],
            "df" => 8.0
          },
          "esvs" => {
            "tf" => 5,
            "positions" => [248, 257, 311, 335, 354],
            "df" => 2.0
          },
          "eukaryotic" => {
            "tf" => 1,
            "positions" => [272],
            "df" => 27.0
          },
          "excretion" => {
            "tf" => 1,
            "positions" => [112],
            "df" => 14.0
          },
          "expression" => {
            "tf" => 3,
            "positions" => [191, 198, 394],
            "df" => 174.0
          },
          "faecally" => {
            "tf" => 1,
            "positions" => [93],
            "df" => 1.0
          },
          "five" => {
            "tf" => 1,
            "positions" => [285],
            "df" => 152.0
          },
          "flagella" => {
            "tf" => 1,
            "positions" => [35],
            "df" => 6.0
          },
          "food" => {
            "tf" => 1,
            "positions" => [95],
            "df" => 79.0
          },
          "form" => {
            "tf" => 2,
            "positions" => [100, 104],
            "df" => 284.0
          },
          "formation" => {
            "tf" => 3,
            "positions" => [316, 330, 350],
            "df" => 60.0
          },
          "formed" => {
            "tf" => 1,
            "positions" => [70],
            "df" => 40.0
          },
          "found" => {
            "tf" => 2,
            "positions" => [469, 489],
            "df" => 417.0
          },
          "four" => {
            "tf" => 2,
            "positions" => [81, 283],
            "df" => 249.0
          },
          "fresh" => {
            "tf" => 1,
            "positions" => [132],
            "df" => 30.0
          },
          "from" => {
            "tf" => 6,
            "positions" => [92, 128, 135, 307, 380, 420],
            "df" => 1284.0
          },
          "g" => {
            "tf" => 2,
            "positions" => [14, 360],
            "df" => 67.0
          },
          "g6pib" => {
            "tf" => 1,
            "positions" => [218],
            "df" => 1.0
          },
          "gastric" => {
            "tf" => 1,
            "positions" => [136],
            "df" => 13.0
          },
          "gene" => {
            "tf" => 2,
            "positions" => [205, 230],
            "df" => 210.0
          },
          "genes" => {
            "tf" => 1,
            "positions" => [202],
            "df" => 161.0
          },
          "giardia" => {
            "tf" => 1,
            "positions" => [1],
            "df" => 18.0
          },
          "giardiasis" => {
            "tf" => 1,
            "positions" => [86],
            "df" => 14.0
          },
          "glucosamine6phosphate" => {
            "tf" => 1,
            "positions" => [216],
            "df" => 2.0
          },
          "golgi" => {
            "tf" => 1,
            "positions" => [267],
            "df" => 3.0
          },
          "growth" => {
            "tf" => 1,
            "positions" => [405],
            "df" => 152.0
          },
          "gt14" => {
            "tf" => 1,
            "positions" => [291],
            "df" => 2.0
          },
          "has" => {
            "tf" => 5,
            "positions" => [28, 115, 433, 462, 475],
            "df" => 1223.0
          },
          "have" => {
            "tf" => 5,
            "positions" => [189, 258, 279, 392, 496],
            "df" => 1281.0
          },
          "hcncp" => {
            "tf" => 3,
            "positions" => [432, 461, 474],
            "df" => 1.0
          },
          "heterooligomer" => {
            "tf" => 1,
            "positions" => [329],
            "df" => 1.0
          },
          "high" => {
            "tf" => 1,
            "positions" => [427],
            "df" => 581.0
          },
          "higher" => {
            "tf" => 1,
            "positions" => [477],
            "df" => 191.0
          },
          "highly" => {
            "tf" => 1,
            "positions" => [177],
            "df" => 301.0
          },
          "homopolymer" => {
            "tf" => 1,
            "positions" => [154],
            "df" => 3.0
          },
          "host" => {
            "tf" => 2,
            "positions" => [47, 143],
            "df" => 503.0
          },
          "hostile" => {
            "tf" => 1,
            "positions" => [109],
            "df" => 4.0
          },
          "hypotonic" => {
            "tf" => 1,
            "positions" => [129],
            "df" => 1.0
          },
          "i" => {
            "tf" => 1,
            "positions" => [374],
            "df" => 165.0
          },
          "identified" => {
            "tf" => 1,
            "positions" => [435],
            "df" => 342.0
          },
          "importance" => {
            "tf" => 1,
            "positions" => [228],
            "df" => 195.0
          },
          "increases" => {
            "tf" => 1,
            "positions" => [219],
            "df" => 61.0
          },
          "infection" => {
            "tf" => 1,
            "positions" => [139],
            "df" => 986.0
          },
          "infective" => {
            "tf" => 1,
            "positions" => [103],
            "df" => 89.0
          },
          "ingested" => {
            "tf" => 1,
            "positions" => [91],
            "df" => 27.0
          },
          "insoluble" => {
            "tf" => 1,
            "positions" => [178],
            "df" => 5.0
          },
          "integral" => {
            "tf" => 2,
            "positions" => [375, 457],
            "df" => 14.0
          },
          "interaction" => {
            "tf" => 1,
            "positions" => [166],
            "df" => 77.0
          },
          "interactions" => {
            "tf" => 1,
            "positions" => [159],
            "df" => 93.0
          },
          "interchain" => {
            "tf" => 1,
            "positions" => [158],
            "df" => 1.0
          },
          "intermolecular" => {
            "tf" => 1,
            "positions" => [320],
            "df" => 2.0
          },
          "intestinal" => {
            "tf" => 2,
            "positions" => [6, 23],
            "df" => 147.0
          },
          "intestine" => {
            "tf" => 3,
            "positions" => [45, 64, 386],
            "df" => 44.0
          },
          "intramolecular" => {
            "tf" => 1,
            "positions" => [318],
            "df" => 2.0
          },
          "introduction" => {
            "tf" => 1,
            "positions" => [0],
            "df" => 1351.0
          },
          "isomeraseb" => {
            "tf" => 1,
            "positions" => [217],
            "df" => 1.0
          },
          "kinetics" => {
            "tf" => 1,
            "positions" => [222],
            "df" => 9.0
          },
          "known" => {
            "tf" => 1,
            "positions" => [184],
            "df" => 423.0
          },
          "lamblia" => {
            "tf" => 3,
            "positions" => [2, 15, 361],
            "df" => 12.0
          },
          "large" => {
            "tf" => 2,
            "positions" => [243, 444],
            "df" => 355.0
          },
          "lead" => {
            "tf" => 2,
            "positions" => [174, 327],
            "df" => 185.0
          },
          "leucinerich" => {
            "tf" => 1,
            "positions" => [287],
            "df" => 1.0
          },
          "level" => {
            "tf" => 1,
            "positions" => [236],
            "df" => 232.0
          },
          "levels" => {
            "tf" => 1,
            "positions" => [192],
            "df" => 257.0
          },
          "like" => {
            "tf" => 1,
            "positions" => [447],
            "df" => 123.0
          },
          "live" => {
            "tf" => 1,
            "positions" => [343],
            "df" => 138.0
          },
          "localization" => {
            "tf" => 1,
            "positions" => [398],
            "df" => 20.0
          },
          "lower" => {
            "tf" => 1,
            "positions" => [63],
            "df" => 111.0
          },
          "lrr" => {
            "tf" => 1,
            "positions" => [497],
            "df" => 1.0
          },
          "lrrs" => {
            "tf" => 2,
            "positions" => [289, 304],
            "df" => 1.0
          },
          "lysis" => {
            "tf" => 1,
            "positions" => [130],
            "df" => 16.0
          },
          "mainly" => {
            "tf" => 1,
            "positions" => [151],
            "df" => 234.0
          },
          "malabsorption" => {
            "tf" => 1,
            "positions" => [18],
            "df" => 8.0
          },
          "many" => {
            "tf" => 2,
            "positions" => [463, 481],
            "df" => 555.0
          },
          "may" => {
            "tf" => 2,
            "positions" => [173, 326],
            "df" => 718.0
          },
          "membrane" => {
            "tf" => 2,
            "positions" => [376, 458],
            "df" => 101.0
          },
          "membranebounded" => {
            "tf" => 1,
            "positions" => [244],
            "df" => 1.0
          },
          "moiety" => {
            "tf" => 1,
            "positions" => [148],
            "df" => 5.0
          },
          "molecular" => {
            "tf" => 1,
            "positions" => [478],
            "df" => 231.0
          },
          "motifs" => {
            "tf" => 4,
            "positions" => [194, 465, 485, 498],
            "df" => 16.0
          },
          "moves" => {
            "tf" => 1,
            "positions" => [32],
            "df" => 4.0
          },
          "nacetylgalactosamine" => {
            "tf" => 1,
            "positions" => [153],
            "df" => 2.0
          },
          "network" => {
            "tf" => 1,
            "positions" => [265],
            "df" => 49.0
          },
          "new" => {
            "tf" => 1,
            "positions" => [142],
            "df" => 496.0
          },
          "nonvariant" => {
            "tf" => 1,
            "positions" => [429],
            "df" => 1.0
          },
          "nterminal" => {
            "tf" => 1,
            "positions" => [280],
            "df" => 20.0
          },
          "nuclei" => {
            "tf" => 3,
            "positions" => [31, 73, 82],
            "df" => 4.0
          },
          "occurs" => {
            "tf" => 1,
            "positions" => [66],
            "df" => 229.0
          },
          "one" => {
            "tf" => 1,
            "positions" => [302],
            "df" => 709.0
          },
          "originally" => {
            "tf" => 1,
            "positions" => [440],
            "df" => 37.0
          },
          "other" => {
            "tf" => 1,
            "positions" => [271],
            "df" => 818.0
          },
          "parasite" => {
            "tf" => 2,
            "positions" => [7, 127],
            "df" => 502.0
          },
          "pathway" => {
            "tf" => 2,
            "positions" => [215, 424],
            "df" => 44.0
          },
          "patterns" => {
            "tf" => 1,
            "positions" => [395],
            "df" => 128.0
          },
          "penetrating" => {
            "tf" => 1,
            "positions" => [22],
            "df" => 9.0
          },
          "peptide" => {
            "tf" => 1,
            "positions" => [299],
            "df" => 33.0
          },
          "peptides" => {
            "tf" => 1,
            "positions" => [282],
            "df" => 32.0
          },
          "place" => {
            "tf" => 1,
            "positions" => [49],
            "df" => 61.0
          },
          "plasmalemma" => {
            "tf" => 1,
            "positions" => [419],
            "df" => 1.0
          },
          "polysaccharide" => {
            "tf" => 3,
            "positions" => [147, 169, 213],
            "df" => 10.0
          },
          "polysaccharides" => {
            "tf" => 2,
            "positions" => [123, 162],
            "df" => 3.0
          },
          "positionally" => {
            "tf" => 1,
            "positions" => [292],
            "df" => 2.0
          },
          "prevalent" => {
            "tf" => 1,
            "positions" => [5],
            "df" => 130.0
          },
          "prevented" => {
            "tf" => 1,
            "positions" => [305],
            "df" => 24.0
          },
          "prevents" => {
            "tf" => 1,
            "positions" => [348],
            "df" => 14.0
          },
          "profiles" => {
            "tf" => 1,
            "positions" => [399],
            "df" => 34.0
          },
          "proliferation" => {
            "tf" => 1,
            "positions" => [53],
            "df" => 51.0
          },
          "properties" => {
            "tf" => 1,
            "positions" => [197],
            "df" => 53.0
          },
          "proposed" => {
            "tf" => 1,
            "positions" => [260],
            "df" => 104.0
          },
          "protease" => {
            "tf" => 1,
            "positions" => [381],
            "df" => 28.0
          },
          "protect" => {
            "tf" => 1,
            "positions" => [125],
            "df" => 43.0
          },
          "protecting" => {
            "tf" => 1,
            "positions" => [378],
            "df" => 11.0
          },
          "protein" => {
            "tf" => 2,
            "positions" => [431, 459],
            "df" => 215.0
          },
          "proteins" => {
            "tf" => 6,
            "positions" => [121, 172, 187, 368, 377, 412],
            "df" => 207.0
          },
          "rarely" => {
            "tf" => 2,
            "positions" => [468, 488],
            "df" => 66.0
          },
          "recently" => {
            "tf" => 1,
            "positions" => [436],
            "df" => 417.0
          },
          "regulation" => {
            "tf" => 1,
            "positions" => [231],
            "df" => 56.0
          },
          "repeats" => {
            "tf" => 1,
            "positions" => [288],
            "df" => 14.0
          },
          "resistant" => {
            "tf" => 1,
            "positions" => [117],
            "df" => 72.0
          },
          "resulting" => {
            "tf" => 1,
            "positions" => [76],
            "df" => 186.0
          },
          "reticulum" => {
            "tf" => 1,
            "positions" => [422],
            "df" => 11.0
          },
          "rich" => {
            "tf" => 1,
            "positions" => [453],
            "df" => 17.0
          },
          "secretory" => {
            "tf" => 1,
            "positions" => [246],
            "df" => 20.0
          },
          "signal" => {
            "tf" => 2,
            "positions" => [281, 298],
            "df" => 35.0
          },
          "similar" => {
            "tf" => 2,
            "positions" => [190, 221],
            "df" => 202.0
          },
          "simultaneously" => {
            "tf" => 1,
            "positions" => [75],
            "df" => 40.0
          },
          "strong" => {
            "tf" => 2,
            "positions" => [157, 165],
            "df" => 104.0
          },
          "subcellular" => {
            "tf" => 1,
            "positions" => [397],
            "df" => 2.0
          },
          "suggesting" => {
            "tf" => 1,
            "positions" => [226],
            "df" => 97.0
          },
          "suitable" => {
            "tf" => 1,
            "positions" => [50],
            "df" => 69.0
          },
          "surface" => {
            "tf" => 2,
            "positions" => [367, 411],
            "df" => 113.0
          },
          "survival" => {
            "tf" => 1,
            "positions" => [107],
            "df" => 96.0
          },
          "switch" => {
            "tf" => 1,
            "positions" => [402],
            "df" => 12.0
          },
          "synchronous" => {
            "tf" => 1,
            "positions" => [30],
            "df" => 1.0
          },
          "tandem" => {
            "tf" => 1,
            "positions" => [286],
            "df" => 15.0
          },
          "targeting" => {
            "tf" => 1,
            "positions" => [308],
            "df" => 85.0
          },
          "them" => {
            "tf" => 1,
            "positions" => [379],
            "df" => 152.0
          },
          "three" => {
            "tf" => 2,
            "positions" => [183, 277],
            "df" => 398.0
          },
          "transcriptional" => {
            "tf" => 1,
            "positions" => [233],
            "df" => 31.0
          },
          "transgolgi" => {
            "tf" => 1,
            "positions" => [264],
            "df" => 4.0
          },
          "translational" => {
            "tf" => 1,
            "positions" => [235],
            "df" => 8.0
          },
          "transmission" => {
            "tf" => 1,
            "positions" => [84],
            "df" => 528.0
          },
          "transport" => {
            "tf" => 1,
            "positions" => [250],
            "df" => 33.0
          },
          "transported" => {
            "tf" => 1,
            "positions" => [415],
            "df" => 13.0
          },
          "treatment" => {
            "tf" => 1,
            "positions" => [341],
            "df" => 600.0
          },
          "trophozoite" => {
            "tf" => 3,
            "positions" => [16, 57, 418],
            "df" => 11.0
          },
          "trophozoites" => {
            "tf" => 1,
            "positions" => [362],
            "df" => 21.0
          },
          "two" => {
            "tf" => 1,
            "positions" => [29],
            "df" => 631.0
          },
          "type" => {
            "tf" => 2,
            "positions" => [373, 455],
            "df" => 147.0
          },
          "under" => {
            "tf" => 1,
            "positions" => [108],
            "df" => 222.0
          },
          "unlike" => {
            "tf" => 1,
            "positions" => [472],
            "df" => 36.0
          },
          "upper" => {
            "tf" => 1,
            "positions" => [44],
            "df" => 36.0
          },
          "variant" => {
            "tf" => 1,
            "positions" => [366],
            "df" => 23.0
          },
          "vegetative" => {
            "tf" => 1,
            "positions" => [404],
            "df" => 1.0
          },
          "ventral" => {
            "tf" => 1,
            "positions" => [40],
            "df" => 3.0
          },
          "vesicles" => {
            "tf" => 1,
            "positions" => [247],
            "df" => 14.0
          },
          "via" => {
            "tf" => 1,
            "positions" => [38],
            "df" => 173.0
          },
          "vsp" => {
            "tf" => 1,
            "positions" => [445],
            "df" => 1.0
          },
          "vsps" => {
            "tf" => 5,
            "positions" => [369, 389, 401, 448, 471],
            "df" => 1.0
          },
          "wall" => {
            "tf" => 9,
            "positions" => [68, 118, 180, 186, 212, 254, 314, 338, 357],
            "df" => 46.0
          },
          "water" => {
            "tf" => 2,
            "positions" => [97, 133],
            "df" => 195.0
          },
          "waterborne" => {
            "tf" => 1,
            "positions" => [9],
            "df" => 8.0
          },
          "weight" => {
            "tf" => 1,
            "positions" => [479],
            "df" => 60.0
          },
          "when" => {
            "tf" => 2,
            "positions" => [55, 88],
            "df" => 394.0
          },
          "within" => {
            "tf" => 1,
            "positions" => [242],
            "df" => 380.0
          },
          "without" => {
            "tf" => 1,
            "positions" => [21],
            "df" => 198.0
          }
        }
      end
    end

    initialize_with do
      doc =
        Document.new(
          schema: schema,
          version: version,
          id: id,
          doi: doi,
          externalIds: externalIds,
          license: license,
          licenseUrl: licenseUrl,
          dataSource: dataSource,
          dataSourceUrl: dataSourceUrl,
          dataSourceVersion: dataSourceVersion,
          type: type,
          title: title,
          authors: authors,
          date: date,
          dateElectronic: dateElectronic,
          dateAccepted: dateAccepted,
          dateReceived: dateReceived,
          journal: journal,
          volume: volume,
          number: number,
          pages: pages,
          keywords: keywords,
          tags: tags
        )
      doc.term_vectors = term_vectors&.with_indifferent_access
      doc
    end
  end
end
