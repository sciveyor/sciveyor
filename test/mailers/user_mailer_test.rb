# frozen_string_literal: true

require "test_helper"

class UserMailerTest < ActionMailer::TestCase
  test "reset_password_instructions" do
    task = create(:task)
    task.user.add_dataset(task.datasets[0], "task test name")
    email = UserMailer.job_finished_email(task.user.email, task)

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal [task.user.email], email.to
    assert_equal ["noreply@sciveyor.com"], email.from
    assert_equal "Analysis job complete", email.subject

    assert_includes email.text_part.body.to_s, task.name
    assert_includes email.text_part.body.to_s,
                    task.datasets[0].name_for(task.user)
    assert_includes email.text_part.body.to_s,
                    Rails.application.routes.url_helpers.workflow_fetch_url(
                      host: "sciveyor.com"
                    )
  end
end
