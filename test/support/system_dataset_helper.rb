# frozen_string_literal: true

module SystemDatasetHelper
  def create_dataset(params = {})
    visit search_path
    fill_in "values[]", with: params[:search] || "test"
    find("input[name=\"values[]\"]").send_keys(:enter)

    click_link "Save"
    # I have no idea why the following line is necessary, but sometimes it is on
    # Selenium/headless Firefox
    click_link "Save" if all(".modal-dialog").empty?

    within(".modal-dialog") do
      within(".modal-body") do
        fill_in "name", with: params[:name] || "Integration Dataset"
      end
      within(".modal-footer") { click_button "Create Dataset" }
    end
  end
end
