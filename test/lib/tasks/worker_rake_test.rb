# frozen_string_literal: true

require "test_helper"
require "rake"

module Sciveyor
  module Jobs
    class FailingRakeTest < Base
      def perform(task, options)
        super
        raise Exception, "oh no"
      end
    end

    class LongRakeTest < Base
      def perform(task, options)
        super
        sleep 1000
        raise "never reached"
      end
    end
  end
end

class WorkerRakeTest < ActiveSupport::TestCase
  self.use_transactional_tests = false

  setup do
    # Some glue to make Rake testing work. Thanks to
    # https://robots.thoughtbot.com/test-rake-tasks-like-a-boss
    task_path = File.join("lib", "tasks", "worker")
    loaded_files_excluding_current_rake_file =
      $LOADED_FEATURES.reject do |file|
        file == Rails.root.join("#{task_path}.rake").to_s
      end

    @rake = Rake::Application.new
    Rake.application = @rake
    Rake.application.rake_require(
      task_path,
      [Rails.root.to_s],
      loaded_files_excluding_current_rake_file
    )

    Rake::Task.define_task(:environment)
  end

  # This is only a partial test; we just want to be sure that the worker hooks
  # for cleaning up after itself are actually doing their jobs, so that we
  # don't leave stale jobs around.
  test "should destroy failing jobs" do
    # Create the task
    task = create(:task, type: "FailingRakeTest")
    task.job_start

    refute task.finished
    refute task.failed

    # Call the Rake runner, which should succeed, though the task failed
    @rake["sciveyor:jobs:analyze_one"].invoke

    # But it should have set the task's failed bit and destroyed the job
    assert task.reload.finished
    assert task.failed
    assert_equal "oh no", task.job_message

    # And there's no more jobs
    assert_equal 0, Job.count
  ensure
    # Clean up after ourselves
    Job.delete_all
    task.datasets.each { |d| d.destroy }
    task.user.destroy
    task.destroy
  end

  test "should destroy long-running jobs" do
    # Create the task
    task = create(:task, type: "LongRakeTest")
    task.job_start

    # Call the Rake runner, which should succeed, though the task failed
    @rake["sciveyor:jobs:analyze_one"].invoke

    # But it should have set the task's failed bit and destroyed the job
    assert task.reload.finished
    assert task.failed
    assert_equal "Job worker ran past maximum allowed time", task.job_message

    # And there's no more jobs
    assert_equal 0, Job.count
  ensure
    # Clean up after ourselves
    Job.delete_all
    task.datasets.each { |d| d.destroy }
    task.user.destroy
    task.destroy
  end

  test "should destroy jobs with too many attempts" do
    # Create the task
    task = create(:task, type: "ArticleDates")
    task.job_start

    # Raise attempts and re-save (have to do this here, job_start will reset it)
    task.attempts = 1000
    task.save

    # Call the Rake runner, which should succeed, though the task failed
    @rake["sciveyor:jobs:analyze_one"].invoke

    # But it should have set the task's failed bit and destroyed the job
    assert task.reload.finished
    assert task.failed
    assert_equal "Attempted to run task too many times (1001), giving up",
                 task.job_message

    # And there's no more jobs
    assert_equal 0, Job.count
  ensure
    # Clean up after ourselves
    Job.delete_all
    task.datasets.each { |d| d.destroy }
    task.user.destroy
    task.destroy
  end
end
