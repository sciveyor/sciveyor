# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Solr
    class FacetTest < ActiveSupport::TestCase
      test "initialize with value but no field raises" do
        assert_raises(Virtus::CoercionError) do
          Sciveyor::Solr::Facet.new(value: "W. Shatner", hits: 10)
        end
      end

      test "initialize with field but no value raises" do
        assert_raises(Virtus::CoercionError) do
          Sciveyor::Solr::Facet.new(field: "author", hits: 10)
        end
      end

      test "initialize with field and value but no hits raises" do
        assert_raises(Virtus::CoercionError) do
          Sciveyor::Solr::Facet.new(field: "author", value: "W. Shatner")
        end
      end

      test "initialize with unknown field raises" do
        assert_raises(ArgumentError) do
          Sciveyor::Solr::Facet.new(
            field: "zuzax",
            value: "W. Shatner",
            hits: 10
          )
        end
      end

      test "initialize strips quotes" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"W. Shatner\"",
            hits: 10
          )

        assert_equal "W. Shatner", f.value
      end

      test "initialize builds queries" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"W. Shatner\"",
            hits: 10
          )

        assert_equal "author:W. Shatner", f.query
      end

      test "sort operator works for facets with different hits" do
        f1 =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"W. Shatner\"",
            hits: 10
          )
        f2 =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"P. Stewart\"",
            hits: 20
          )

        assert f2 < f1
      end

      test "sort operator sorts alphabetically for same hits on authors" do
        f1 =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"W. Shatner\"",
            hits: 10
          )
        f2 =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"P. Stewart\"",
            hits: 10
          )

        assert f1 > f2
      end

      test "sort operator sorts years inverse for same hits on years" do
        f1 =
          Sciveyor::Solr::Facet.new(
            field: :date,
            value: "2000-01-01T00:00:00Z",
            hits: 10
          )
        f2 =
          Sciveyor::Solr::Facet.new(
            field: :date,
            value: "1980-01-01T00:00:00Z",
            hits: 10
          )

        assert f2 > f1
      end
    end
  end
end
