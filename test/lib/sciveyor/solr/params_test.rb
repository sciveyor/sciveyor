# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Solr
    class ParamsTest < ActiveSupport::TestCase
      test "params eliminates blank params" do
        params = { fields: [""], values: [""] }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "*:*", ret.q
      end

      test "params fails if given a bad facet type" do
        params = { facets: ["thing:blue"] }
        params = ActionController::Parameters.new(params)
        assert_raises(ActionController::BadRequest) do
          Sciveyor::Solr::Params.new(params: params)
        end
      end

      test "params fails if given an empty value" do
        params = { facets: ["author:\"\""] }
        params = ActionController::Parameters.new(params)
        assert_raises(ActionController::BadRequest) do
          Sciveyor::Solr::Params.new(params: params)
        end
      end

      test "params puts together empty search correctly" do
        params = {}
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "*:*", ret.q
      end

      test "params copies raw Solr search content correctly" do
        params = { solr: "test" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "test", ret.q
      end

      test "params combines the search terms with the boolean values" do
        params = {
          boolean: "and",
          fields: %w[volume number pages],
          values: %w[30 5 300-301]
        }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "volume:(30) AND number:(5) AND pages:(300-301)", ret.q
      end

      test "params ignores offset and limit for non-API searches" do
        params = { offset: "1", limit: "20" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_nil ret.offset
        assert_nil ret.limit
      end

      test "params sets the initial cursor mark for non-API searches" do
        params = { fields: ["dismax"], values: ["test"] }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_nil ret.cursor_mark
        assert_equal "*", ret.to_solr[:cursor_mark]
      end

      test "params copies the non-initial cursor mark for non-API searches" do
        params = { fields: ["dismax"], values: ["test"], cursor_mark: "asdf" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "asdf", ret.cursor_mark
      end

      test "params sorts by year, descending, by default" do
        params = {}
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "date desc,id asc", ret.to_solr[:sort]
      end

      test "params sorts by year, descending, with just a facet query" do
        params = { fq: ["journal:\"Journal of Nothing\""] }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "date desc,id asc", ret.to_solr[:sort]
      end

      test "params sorts by score, descending, for a basic dismax search" do
        params = { fields: ["dismax"], values: ["testing"] }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "score desc,id asc", ret.to_solr[:sort]
      end

      test "params passes through explicit sorts" do
        params = { sort: "volume desc" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params)

        assert_equal "volume desc", ret.sort

        # This is an important test: because this is not an API search, the
        # to_solr method needs to add an id sort here for the cursor mark
        # to work.
        assert_equal "volume desc,id asc", ret.to_solr[:sort]
      end

      test "params successfully parses offset and limit parameters for API searches" do
        params = { offset: "20", limit: "20" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params, api: true)

        assert_equal 20, ret.offset
        assert_equal 20, ret.limit
      end

      test "params drops non-integral offset values for API searches" do
        params = { offset: "zzyzzy", limit: "20" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params, api: true)

        assert_nil ret.offset
        assert_equal 20, ret.limit
      end

      test "params drops non-integer limit values for API searches" do
        params = { offset: "10", limit: "zzyzzy" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params, api: true)

        assert_equal 10, ret.offset
        assert_nil ret.limit
      end

      test "params rounds up zero limit for API searches" do
        params = { offset: "10", limit: "0" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params, api: true)

        assert_equal 10, ret.offset
        assert_equal 1, ret.limit
      end

      test "params does not include a cursor mark by default for API searches" do
        params = {}
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params, api: true)

        assert_nil ret.cursor_mark
      end

      test "params does not include a cursor mark even when passed one for API searches" do
        params = { cursor_mark: "asdf" }
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params, api: true)

        assert_nil ret.cursor_mark
      end

      test "params does not include ID in the sort fields for API searches" do
        params = {}
        params = ActionController::Parameters.new(params)
        ret = Sciveyor::Solr::Params.new(params: params, api: true)

        assert_equal "date desc", ret.to_solr[:sort]
      end

      test "dataset copies over facets and boolean" do
        category =
          Category.create(
            name: "Test Category",
            journals: ["Gutenberg", "PLoS Neglected Tropical Diseases"]
          )
        fq = [
          Sciveyor::Solr::Params::FilterQuery.categories_fq_string(
            [category.to_param]
          )
        ]
        dataset = create(:full_dataset, fq: fq, boolean: "or")

        ret = Sciveyor::Solr::Params.new(dataset: dataset)

        assert_equal fq, ret.fq
        assert_equal "or", ret.boolean
      end

      test "dataset reconstructs fields and values" do
        dataset = create(:full_dataset)
        ret = Sciveyor::Solr::Params.new(dataset: dataset)

        assert_equal 1, ret.fields.size
        assert_equal 1, ret.values.size
        assert_equal "id", ret.fields[0]
        assert_includes ret.values[0], "10.1371/"
        assert_includes ret.values[0], " OR "
      end

      test "dataset reconstructs facets" do
        dataset = create(:full_dataset)
        dataset.fq = [
          "{!parent which=\"type:article\"}(+name:\"Jane Doe\")",
          "+type:article +journal:\"Journal of Test\"",
          "+type:article +date:[2020-01-01T00:00:00Z TO *]"
        ]
        ret = Sciveyor::Solr::Params.new(dataset: dataset)

        assert_equal 3, ret.facets.size
        assert_equal "author:Jane Doe", ret.facets[0]
        assert_equal "journal:Journal of Test", ret.facets[1]
        assert_equal "date:after", ret.facets[2]
      end

      test "dataset reconstructs categories" do
        category =
          Category.create(
            name: "Test Category",
            journals: ["Gutenberg", "PLoS Neglected Tropical Diseases"]
          )
        fq = [
          Sciveyor::Solr::Params::FilterQuery.categories_fq_string(
            [category.to_param]
          )
        ]
        dataset = create(:full_dataset, fq: fq, boolean: "or")
        ret = Sciveyor::Solr::Params.new(dataset: dataset)

        assert_equal 1, ret.categories.size
        assert_equal category.id, ret.categories[0]
      end

      test "dataset reconstructs facets and categories" do
        category =
          Category.create(
            name: "Test Category",
            journals: ["Gutenberg", "PLoS Neglected Tropical Diseases"]
          )
        dataset = create(:full_dataset)
        dataset.fq = [
          "+type:article +journal:(\"Gutenberg\" OR \"PLoS Neglected Tropical Diseases\") /* categories:#{category.id} */",
          "+type:article +journal:\"PNTDS\"",
          "{!parent which=\"type:article\"}(+name:\"Jane Doe\")"
        ]
        ret = Sciveyor::Solr::Params.new(dataset: dataset)

        assert_equal 2, ret.facets.size
        assert_equal "journal:PNTDS", ret.facets[0]
        assert_equal "author:Jane Doe", ret.facets[1]
        assert_equal 1, ret.categories.size
        assert_equal category.id, ret.categories[0]
      end

      test "to_solr includes expected parameters for normal search" do
        params = { fields: ["dismax"], values: ["test"] }
        params = ActionController::Parameters.new(params)
        search_params = Sciveyor::Solr::Params.new(params: params)
        ret = search_params.to_solr

        assert_equal " {!edismax v=\"test\"}", ret[:q]
        assert_equal "score desc,id asc", ret[:sort]
        assert_equal 20, ret[:rows]
        assert_equal "*", ret[:cursor_mark]
        assert_equal 0, ret[:start]
        assert_nil ret[:fq]
      end

      test "to_solr includes expected parameters for API search" do
        params = {
          fields: ["dismax"],
          values: ["test"],
          limit: "20",
          offset: "30"
        }
        params = ActionController::Parameters.new(params)
        search_params = Sciveyor::Solr::Params.new(params: params, api: true)
        ret = search_params.to_solr

        assert_equal " {!edismax v=\"test\"}", ret[:q]
        assert_equal "score desc", ret[:sort]
        assert_equal 20, ret[:rows]
        assert_equal 30, ret[:start]
        assert_nil ret[:fq]
        assert_nil ret[:cursor_mark]
      end

      test "to_solr includes expected parameters for dataset" do
        category =
          Category.create(
            name: "Test Category",
            journals: ["Gutenberg", "PLoS Neglected Tropical Diseases"]
          )
        fq = [
          Sciveyor::Solr::Params::FilterQuery.categories_fq_string(
            [category.to_param]
          )
        ]
        dataset = create(:full_dataset, fq: fq, boolean: "or")
        search_params = Sciveyor::Solr::Params.new(dataset: dataset)
        ret = search_params.to_solr

        assert_equal dataset.q[0], ret[:q]
        assert_equal fq, ret[:fq]
        assert_equal "score desc,id asc", ret[:sort]
        assert_equal 20, ret[:rows]
        assert_equal "*", ret[:cursor_mark]
        assert_equal 0, ret[:start]
      end

      test "to_dataset includes expected parameters for normal search" do
        params = { fields: ["dismax"], values: ["test"] }
        params = ActionController::Parameters.new(params)
        search_params = Sciveyor::Solr::Params.new(params: params)
        ret = search_params.to_dataset

        assert_equal " {!edismax v=\"test\"}", ret[:q][0]
        assert_equal "and", ret[:boolean]
        assert_equal [], ret[:fq]
      end

      test "to_dataset includes expected parameters for dataset" do
        category =
          Category.create(
            name: "Test Category",
            journals: ["Gutenberg", "PLoS Neglected Tropical Diseases"]
          )
        fq = [
          Sciveyor::Solr::Params::FilterQuery.categories_fq_string(
            [category.to_param]
          )
        ]
        dataset = create(:full_dataset, fq: fq, boolean: "or")
        search_params = Sciveyor::Solr::Params.new(dataset: dataset)
        ret = search_params.to_dataset

        assert_equal dataset.q[0], ret[:q][0]
        assert_equal dataset.boolean, ret[:boolean]
        assert_equal dataset.fq, ret[:fq]
      end
    end
  end
end
