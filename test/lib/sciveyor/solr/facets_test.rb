# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Solr
    class FacetsTest < ActiveSupport::TestCase
      setup do
        solr_response = build(:solr_response).response
        rsolr = RSolr::Ext::Response::Base.new(solr_response, "search", nil)

        @facets = Sciveyor::Solr::Facets.new(solr_response["facets"])
      end

      test "search_params works for empty set of facets" do
        p =
          ActionController::Parameters.new(
            controller: "search",
            action: "index",
            facets: ["author:Pei-Wei Chiu"]
          )
        sp = Sciveyor::Solr::Params.new(params: p)

        out = Sciveyor::Solr::Facets.search_params(sp, [])
        assert_nil out.fq
      end

      test "search_params works for a facet" do
        p =
          ActionController::Parameters.new(
            controller: "search",
            action: "index"
          )
        sp = Sciveyor::Solr::Params.new(params: p)
        f = @facets.for_field(:author).find { |o| o.value == "Pei-Wei Chiu" }

        out = Sciveyor::Solr::Facets.search_params(sp, [f])
        assert_equal 1, out.facets.count
        assert_equal "author:Pei-Wei Chiu", out.facets[0]
      end

      test "active works when empty" do
        p =
          ActionController::Parameters.new(
            controller: "search",
            action: "index"
          )
        sp = Sciveyor::Solr::Params.new(params: p)

        assert_empty @facets.active(sp)
      end

      test "active works for a facet" do
        p =
          ActionController::Parameters.new(
            controller: "search",
            action: "index",
            facets: ["author:Pei-Wei Chiu"]
          )
        sp = Sciveyor::Solr::Params.new(params: p)
        f = @facets.for_field(:author).find { |o| o.value == "Pei-Wei Chiu" }

        assert_equal [f], @facets.active(sp)
      end

      test "for_field has the right hash keys" do
        refute_empty @facets.for_field(:author)
        refute_empty @facets.for_field(:journal)
        refute_empty @facets.for_field(:date)
      end

      test "for_field parses author correctly" do
        f = @facets.for_field(:author).find { |o| o.value == "Pei-Wei Chiu" }

        refute_nil f
        assert_equal 1, f.hits
      end

      test "for_field does not include entries for non-existent authors" do
        f = @facets.for_field(:author).find { |o| o.value == "W. Shatner" }

        assert_nil f
      end

      test "for_field parses journal correctly" do
        f =
          @facets
            .for_field(:journal)
            .find { |o| o.value == "PLoS Neglected Tropical Diseases" }

        refute_nil f
        assert_equal 1, f.hits
      end

      test "for_field does not include entries for non-existent journals" do
        f =
          @facets
            .for_field(:journal)
            .find { |o| o.value == "Journal of Nothing" }

        assert_nil f
      end

      test "for_field parses date correctly" do
        f = @facets.for_field(:date).find { |o| o.value == "before" }

        refute_nil f
        assert_equal 1, f.hits
      end

      test "for_field does not include entries for non-present years" do
        f = @facets.for_field(:date).find { |o| o.value == "1940–1949" }

        assert_nil f
      end

      test "sorted_for_field works" do
        s = @facets.sorted_for_field(:date)

        assert_equal "after", s[0].value
        assert_equal "2010-01-01T00:00:00Z", s[1].value
        assert_equal "before", s[2].value
      end

      test "for_query works" do
        refute_nil @facets.for_query("date:2010")
        refute_nil @facets.for_query("author:Pei-Wei Chiu")
      end

      test "empty works" do
        assert Sciveyor::Solr::Facets.new(nil).empty?
      end
    end
  end
end
