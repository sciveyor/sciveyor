# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Presenters
    class TestView
      include ActionView::Helpers::FormHelper
      include ActionView::Helpers::FormOptionsHelper
      include ActionView::Helpers::TagHelper
      include ActionView::Context
    end

    class BootstrapFormBuilderTest < ActionView::TestCase
      test "validation errors with only server error" do
        user = create(:user)
        user.stubs(:errors).returns(email: ["server error"])
        builder = BootstrapFormBuilder.new(:user, user, TestView.new, {})

        html = builder.text_field(:email)
        matcher = Capybara.string(html)
        assert matcher.has_selector?("span.server-errors", text: "server error")
      end

      test "validation errors with only client error and no message" do
        user = create(:user)
        builder = BootstrapFormBuilder.new(:user, user, TestView.new, {})

        html = builder.text_field(:email, input_options: { required: true })
        matcher = Capybara.string(html)
        assert matcher.has_selector?(
                 "span.client-errors:not([style])",
                 text: "You must enter an email address"
               )
      end

      test "validation errors with class symbol and no message" do
        builder = BootstrapFormBuilder.new(:user, nil, TestView.new, {})

        html = builder.text_field(:email, input_options: { required: true })
        matcher = Capybara.string(html)
        assert matcher.has_selector?(
                 "span.client-errors:not([style])",
                 text: "You must enter an email address"
               )
      end

      test "validation errors with client error and message" do
        builder = BootstrapFormBuilder.new(:user, nil, TestView.new, {})

        html =
          builder.text_field(
            :email,
            input_options: {
              required: true
            },
            client_side_message: "message thing"
          )
        matcher = Capybara.string(html)
        assert matcher.has_selector?(
                 "span.client-errors:not([style])",
                 text: "message thing"
               )
      end

      test "validation errors with client and server errors" do
        user = create(:user)
        user.stubs(:errors).returns(email: ["server error"])
        builder = BootstrapFormBuilder.new(:user, user, TestView.new, {})

        html = builder.text_field(:email, input_options: { required: true })
        matcher = Capybara.string(html)

        assert matcher.has_selector?("span.server-errors", text: "server error")
        assert matcher.has_selector?(
                 "span.client-errors",
                 text: "You must enter an email address",
                 visible: false
               )
      end
    end
  end
end
