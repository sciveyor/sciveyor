# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Presenters
    class SearchResultPresenterTest < ActiveSupport::TestCase
      test "next_page_params returns nil without another page" do
        r = stub(solr_response: { "nextCursorMark" => "mark" }, num_hits: 1)
        p = ActionController::Parameters.new(cursor_mark: "mark")
        sp = Sciveyor::Solr::Params.new(params: p)
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_nil pres.next_page_params(sp)
      end

      test "next_page_params moves mark" do
        r = stub(solr_response: { "nextCursorMark" => "mark_new" }, num_hits: 1)
        p = ActionController::Parameters.new(cursor_mark: "mark")
        sp = Sciveyor::Solr::Params.new(params: p)
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "mark_new", pres.next_page_params(sp).cursor_mark
      end

      test "num_hits_string returns \"in database\" without search" do
        r = stub(num_hits: 1000, params: {})
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "1,000 articles in database", pres.num_hits_string
      end

      test "num_hits_string returns \"found\" with search" do
        r = stub(num_hits: 1000, params: { q: "Test search" })
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "1,000 articles found", pres.num_hits_string
      end

      test "num_hits_string returns \"found\" with faceting" do
        r = stub(num_hits: 1000, params: { fq: ["something here"] })
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "1,000 articles found", pres.num_hits_string
      end

      test "num_hits_string respects locales" do
        I18n.locale = :fr
        I18n.backend.store_translations(
          :fr,
          search: {
            index: {
              num_hits_found: {
                other: "%{count} articles trouvés"
              }
            }
          }
        )

        r = stub(num_hits: 1000, params: { fq: ["something here"] })
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "1 000 articles trouvés", pres.num_hits_string

        I18n.locale = :en
      end

      test "current_sort_method works" do
        r = stub(params: { "sort" => "score desc" })
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "Sort: Relevance", pres.current_sort_method
      end

      test "current_sort_method works for unknown methods" do
        r = stub(params: { "sort" => "nope desc" })
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "Unknown", pres.current_sort_method
      end

      test "sort_methods works" do
        r = stub(params: { "sort" => "score desc" })
        pres = Sciveyor::Presenters::SearchResultPresenter.new(result: r)

        assert_equal "Sort: Relevance", pres.sort_methods.assoc("score desc")[1]
        assert_equal "Sort: Title (ascending)",
                     pres.sort_methods.assoc("title asc")[1]
        assert_equal "Sort: Journal (descending)",
                     pres.sort_methods.assoc("journal desc")[1]
        assert_equal "Sort: Publication date (ascending)",
                     pres.sort_methods.assoc("date asc")[1]
      end
    end
  end
end
