# frozen_string_literal: true

require "test_helper"
require_relative "./common_tests"

# This tests both halves of the RDF serializers, the same code is generating
# the N3 representation, but this is easier to spec.
module Sciveyor
  module Documents
    module Serializers
      class RdfXmlTest < ActiveSupport::TestCase
        include CommonTests

        test "single document serialization" do
          doc = build(:full_document)
          raw = Sciveyor::Documents::Serializers::RdfXml.new(doc).serialize
          xml = Nokogiri::XML::Document.parse(raw)

          assert_equal "rdf", xml.root.name

          assert_equal 1, xml.css("Description").size

          assert_equal "A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia",
                       xml.at_css("dc|title").content
          assert_equal "PLoS Neglected Tropical Diseases",
                       xml.at_css("dc|relation").content
          assert_equal "2010-05-01T00:00:00+00:00",
                       xml.at_css("dc|issued").content
          assert_equal "Journal Article", xml.at_css("dc|type").content
          assert_equal "info:doi/10.1371/journal.pntd.0000677",
                       xml.at_css("dc|identifier").content

          tags = xml.css("dc|creator")
          assert_equal 5, tags.size
          expected = [
            "Chiu, Pei-Wei",
            "Huang, Yu-Chang",
            "Pan, Yu-Jiao",
            "Wang, Chih-Hung",
            "Sun, Chin-Hung"
          ]
          assert_equal expected.sort, tags.map(&:content).sort

          expected = [
            "&ctx_ver=Z39.88-2004&rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&rft.genre=article&rft_id=info:doi%2F10.1371%2Fjournal.pntd.0000677&rft.atitle=A+Novel+Family+of+Cyst+Proteins+with+Epidermal+Growth+Factor+Repeats+in+Giardia+lamblia&rft.title=PLoS+Neglected+Tropical+Diseases&rft.date=2010-05-01&rft.volume=4&rft.issue=5&rft.spage=e677&rft.aufirst=Pei-Wei&rft.aulast=Chiu&rft.au=Yu-Chang+Huang&rft.au=Yu-Jiao+Pan&rft.au=Chih-Hung+Wang&rft.au=Chin-Hung+Sun",
            "PLoS Neglected Tropical Diseases 4(5), e677. (2010)"
          ]

          citations = xml.css("dc|bibliographicCitation")
          assert_equal 2, citations.size

          assert_equal expected.sort, citations.map(&:content).sort
        end

        test "array serialization" do
          doc = build(:full_document)
          doc2 = build(:full_document, id: "doi:10.0000/fakedoi")
          docs = [doc, doc2]
          raw = Sciveyor::Documents::Serializers::RdfXml.new(docs).serialize
          xml = Nokogiri::XML::Document.parse(raw)

          assert_equal 2, xml.css("Description").size
        end
      end
    end
  end
end
