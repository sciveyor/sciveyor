# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Documents
    module Serializers
      class BaseTest < ActiveSupport::TestCase
        test "available works" do
          list = Sciveyor::Documents::Serializers::Base.available

          assert_kind_of Array, list
          assert_includes list, :bibtex
        end

        test "for works" do
          Sciveyor::Documents::Serializers::Base.available.each do |ser|
            assert_kind_of Class,
                           Sciveyor::Documents::Serializers::Base.for(ser)
          end
        end
      end
    end
  end
end
