# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Documents
    class WordListTest < ActiveSupport::TestCase
      setup do
        # Just always return the same document stub
        @doc = build(:full_document)
        Document.stubs(:find_by!).returns(@doc)
      end

      test "with 1-grams" do
        lister = Sciveyor::Documents::WordList.new
        list = lister.words_for(@doc.id)

        assert_equal %w[introduction giardia lamblia prevalent intestinal],
                     list.take(5)
        assert_equal 215, lister.corpus_dfs["protein"]
      end

      test "with 2-grams" do
        lister = Sciveyor::Documents::WordList.new(ngrams: 2)
        list = lister.words_for(@doc.id)

        assert_equal "introduction giardia", list.first
        assert_equal "giardia lamblia", list.second

        list.each { |g| assert_equal 2, g.split.size }
      end

      test "with stemming" do
        lister = Sciveyor::Documents::WordList.new(stemming: :stem)
        list = lister.words_for(@doc.id)

        assert_includes list, "introduct"
        refute_includes list, "introduction"

        refute_nil lister.corpus_dfs["introduct"]
      end

      test "with lemmatization" do
        lister = Sciveyor::Documents::WordList.new(stemming: :lemma)
        list = lister.words_for(@doc.id)

        assert_includes list, "be"
        refute_includes list, "is"
        refute_includes list, "was"
      end
    end
  end
end
