# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Documents
    class AuthorTest < ActiveSupport::TestCase
      # Correctly deal with splitting by boolean operators and removing parentheses,
      # when needed
      def query_to_array(str)
        return [str[1..-2]] unless str[0] == "("
        str[1..-2].split(" OR ").map { |n| n[1..-2] }
      end

      test "name should ignore complex values" do
        au =
          Sciveyor::Documents::Author.new(
            name: "for the Leishmaniasis East Africa Platform (LEAP) group"
          )
        assert_equal "for the Leishmaniasis East Africa Platform (LEAP) group",
                     au.name
      end

      test "name should return the parameter string without parsing" do
        au =
          Sciveyor::Documents::Author.new(
            name: "Greebleflotz, Johannes van der 123 Jr."
          )
        assert_equal "Greebleflotz, Johannes van der 123 Jr.", au.name
      end

      test "to_s should be the same as name" do
        au =
          Sciveyor::Documents::Author.new(
            name: "Greebleflotz, Johannes van der 123 Jr."
          )
        assert_equal au.name, au.to_s
      end

      test "no parsing is done if a part is passed" do
        au =
          Sciveyor::Documents::Author.new(name: "Asdf von Sdfg", prefix: "von")
        assert_nil au.first
        assert_nil au.last
        assert_nil au.middle
        assert_nil au.suffix
      end

      test "first should return nothing with only one name" do
        au = Sciveyor::Documents::Author.new(name: "Asdf")
        assert_nil au.first
      end

      test "first returns the first bit, with no comma" do
        au = Sciveyor::Documents::Author.new(name: "Asdf Sdfg")
        assert_equal "Asdf", au.first
      end

      test "first returns the last bit, with comma" do
        au = Sciveyor::Documents::Author.new(name: "Sdfg, Asdf")
        assert_equal "Asdf", au.first
      end

      test "last returns the name with one name" do
        au = Sciveyor::Documents::Author.new(name: "Asdf")
        assert_equal "Asdf", au.last
      end

      test "last returns the last bit, with no comma" do
        au = Sciveyor::Documents::Author.new(name: "Asdf Sdfg")
        assert_equal "Sdfg", au.last
      end

      test "last returns the first bit, with comma" do
        au = Sciveyor::Documents::Author.new(name: "Sdfg, Asdf")
        assert_equal "Sdfg", au.last
      end

      test "prefix returns as expected with no comma" do
        au = Sciveyor::Documents::Author.new(name: "Asdf van der Sdfg")
        assert_equal "van der", au.prefix
      end

      test "prefix returns as expected with comma" do
        au = Sciveyor::Documents::Author.new(name: "Van der Sdfg, Asdf")
        assert_equal "Van der", au.prefix
      end

      test "suffix returns as expected with comma" do
        # N.B.: the BibTeX::Names parser does not pull out suffixes without comma
        au = Sciveyor::Documents::Author.new(name: "van der Sdfg, Jr., Asdf")
        assert_equal "Jr.", au.suffix
      end

      test "everything works if BibTeX fails" do
        BibTeX::Name.expects(:parse).with("testy testerson").returns(nil)
        au = Sciveyor::Documents::Author.new(name: "testy testerson")

        assert_nil au.first
        assert_nil au.last
        assert_nil au.prefix
        assert_nil au.suffix

        assert_equal "testy testerson", au.name
      end

      test "to_lucene works for Last" do
        expected = ["Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(name: "Last").to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for F Last" do
        expected = ["F* Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(name: "F Last").to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for FMM Last" do
        expected = ["F* Last", "F* M* M* Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(name: "FMM Last").to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First Last" do
        expected = ["F Last", "First Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(name: "First Last").to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First M M Last" do
        expected = ["F M* M* Last", "First M* M* Last", "First Last", "F Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(name: "First M M Last").to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First MM Last" do
        expected = ["F M* M* Last", "First M* M* Last", "First Last", "F Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(name: "First MM Last").to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First Middle Middle Last" do
        expected = [
          "First Last",
          "F Last",
          "First Middle Middle Last",
          "First Middle M Last",
          "First M Middle Last",
          "First M M Last",
          "First MM Last",
          "F Middle Middle Last",
          "F Middle M Last",
          "F M Middle Last",
          "FM Middle Last",
          "F M M Last",
          "FMM Last",
          "FM M Last",
          "F MM Last"
        ]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(
              name: "First Middle Middle Last"
            ).to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for F Last (parts)" do
        expected = ["F* Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(
              name: "F Last",
              first: "F",
              last: "Last"
            ).to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for FMM Last (parts)" do
        expected = ["F* Last", "F* M* M* Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(
              name: "FMM Last",
              first: "FMM",
              last: "Last"
            ).to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First Last (parts)" do
        expected = ["F Last", "First Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(
              name: "First Last",
              first: "First",
              last: "Last"
            ).to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First M M Last (parts)" do
        expected = ["F M* M* Last", "First M* M* Last", "First Last", "F Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(
              name: "First M M Last",
              first: "First",
              middle: "M M",
              last: "Last"
            ).to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First MM Last (parts)" do
        expected = ["F M* M* Last", "First M* M* Last", "First Last", "F Last"]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(
              name: "First MM Last",
              first: "First",
              middle: "MM",
              last: "Last"
            ).to_lucene
          )

        assert_equal expected.sort, actual.sort
      end

      test "to_lucene works for First Middle Middle Last (parts)" do
        expected = [
          "First Last",
          "F Last",
          "First Middle Middle Last",
          "First Middle M Last",
          "First M Middle Last",
          "First M M Last",
          "First MM Last",
          "F Middle Middle Last",
          "F Middle M Last",
          "F M Middle Last",
          "FM Middle Last",
          "F M M Last",
          "FMM Last",
          "FM M Last",
          "F MM Last"
        ]
        actual =
          query_to_array(
            Sciveyor::Documents::Author.new(
              name: "First Middle Middle Last",
              first: "First",
              middle: "Middle Middle",
              last: "Last"
            ).to_lucene
          )

        assert_equal expected.sort, actual.sort
      end
    end
  end
end
