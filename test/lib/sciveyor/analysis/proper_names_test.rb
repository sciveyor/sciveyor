# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Analysis
    class ProperNamesTest < ActiveSupport::TestCase
      test "works" do
        refs =
          Sciveyor::Analysis::ProperNames.call(dataset: create(:full_dataset))

        refute_empty refs
      end
    end
  end
end
