# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Analysis
    class StopListTest < ActiveSupport::TestCase
      test "available works" do
        ret = Sciveyor::Analysis::StopList.available

        assert_kind_of Array, ret
        assert_not ret.empty?
        assert_includes ret, :en
      end

      test "for works" do
        ret = Sciveyor::Analysis::StopList.for(:en)

        assert_kind_of Array, ret
        assert_not ret.empty?
        assert_includes ret, "the"
      end

      test "for returns nil for missing language" do
        assert_nil Sciveyor::Analysis::StopList.for(:xx)
      end
    end
  end
end
