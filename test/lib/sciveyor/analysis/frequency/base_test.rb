# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Analysis
    module Frequency
      class BaseTest < ActiveSupport::TestCase
        test "creates FromTf when available" do
          analyzer =
            Sciveyor::Analysis::Frequency::Base.call(
              dataset: create(:full_dataset)
            )

          assert_kind_of Sciveyor::Analysis::Frequency::FromTf, analyzer
        end

        test "creates FromPosition otherwise" do
          analyzer =
            Sciveyor::Analysis::Frequency::Base.call(
              :dataset => create(:full_dataset),
              "num_blocks" => 3,
              "ngrams" => 2
            )

          assert_kind_of Sciveyor::Analysis::Frequency::FromPosition, analyzer
        end
      end
    end
  end
end
