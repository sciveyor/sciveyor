# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class TermDatesTest < ActiveSupport::TestCase
      def perform
        dataset = create(:full_dataset, num_docs: 1)
        @task = create(:task, type: "TermDates", datasets: [dataset])
        run_job_now(@task, "term" => "disease")
      end

      include JobHelper

      test "should need one dataset" do
        assert_equal 1, TermDates.num_datasets
      end

      test "should work" do
        task =
          create(:task, type: "TermDates", datasets: [create(:full_dataset)])

        # Add another article from a much later year, so that we get some
        # intervening zeros
        task.datasets[0].q << "id:\"doi:10.1371/journal.pntd.0001716\""
        task.datasets[0].boolean = "or"
        task.datasets[0].save

        run_job_now(task, "term" => "disease")

        assert_equal "Plot word occurrences by date", task.reload.name
        assert_equal 2, task.uploads.count
        refute_nil task.upload_for("application/json")
        refute_nil task.upload_for("text/csv")

        data = JSON.parse(task.json)
        assert_kind_of Hash, data

        # Data is reasonable
        assert_includes 2009..2012, data["data"][0][0]
        assert_includes 0..30, data["data"][0][1]

        # Fills in intervening years between new and old documents with zeros
        refute_nil(data["data"].find { |y| y[1].zero? })

        # Data is sorted by year
        assert_equal data["data"].sort_by { |d| d[0] }, data["data"]
      end
    end
  end
end
