# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class Mock < Base
      def perform(task, options = {})
        super
      end

      def filename_test(test)
        unique_filename(test)
      end

      def call_dataset
        dataset
      end

      def call_datasets
        datasets
      end
    end

    class BaseTest < ActiveSupport::TestCase
      test "should query the right translation keys" do
        I18n.expects(:t).with("jobs.mock.testing").returns("wat")
        Mock.t(".testing")
      end

      test "should default to true for available?" do
        assert Mock.available?
      end

      test "should return single dataset" do
        task = create(:task, type: "Mock")
        job = Mock.new
        job.perform(task)

        assert_equal task.datasets.first, job.call_dataset
      end

      test "should fail to return multiple datasets through dataset" do
        task = create(:task, type: "Mock")
        job = Mock.new
        dataset2 = create(:dataset)
        task.datasets << dataset2
        job.perform(task)

        assert_raises(ArgumentError) { job.call_dataset }
      end

      test "should fail to return single dataset through datasets" do
        task = create(:task, type: "Mock")
        job = Mock.new
        job.perform(task)

        assert_raises(ArgumentError) { job.call_datasets }
      end

      test "should return multiple datasets" do
        task = create(:task, type: "Mock")
        job = Mock.new
        dataset2 = create(:dataset)
        task.datasets << dataset2
        job.perform(task)

        assert_equal task.datasets, job.call_datasets
      end

      test "should uniquify filenames" do
        j = Mock.new
        fn = "base.json"
        uniq = j.filename_test(fn)

        assert_not_equal fn, uniq
        assert uniq.length > fn.length
      end
    end
  end
end
