# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class ExportCitationsTest < ActiveSupport::TestCase
      def perform
        dataset = create(:full_dataset, num_docs: 1)
        @task = create(:task, type: "ExportCitations", datasets: [dataset])
        run_job_now(@task, "format" => "bibtex")
      end

      include JobHelper

      test "should need one dataset" do
        assert_equal 1, ExportCitations.num_datasets
      end

      test "should raise with invalid format" do
        assert_raises(KeyError) do
          ExportCitations.new.perform(create(:task), format: "notaformat")
        end
      end

      test "should work" do
        task =
          create(
            :task,
            type: "ExportCitations",
            datasets: [create(:full_dataset, num_docs: 10)]
          )

        run_job_now(task, "format" => "bibtex")

        assert_equal "Export dataset as citations", task.reload.name

        # Make sure it made the right number of entries in the ZIP
        Sciveyor::Storage.get(
          task.upload_for("application/zip").filename
        ) do |io, type|
          entries = 0
          ::Zip::InputStream.open(io) do |zis|
            entries += 1 while zis.get_next_entry
          end
          assert_equal 10, entries
        end
      end
    end
  end
end
