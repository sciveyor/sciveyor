# frozen_string_literal: true

require "test_helper"

class SearchApiTest < ActionDispatch::IntegrationTest
  test "should load a basic search" do
    get search_url(format: "json", offset: "10", limit: "10")
    json = JSON.parse(@response.body)

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_equal 1500, json["results"]["num_hits"]
    assert_equal 10, json["results"]["documents"].size

    assert_equal "*:*", json["results"]["solr_params"]["q"]
    assert_equal "10", json["results"]["solr_params"]["start"]
    assert_equal "10", json["results"]["solr_params"]["rows"]
  end

  test "should load a faceted search" do
    get search_url(format: "json", facets: ["author:Alan Fenwick"])
    json = JSON.parse(@response.body)

    assert_response :success
    assert_equal 9, json["results"]["num_hits"]

    # This is a test for a fairly internal piece of behavior, but it ensures
    # that we converted the 'facets' parameter to an fq as expected.
    assert_equal "{!parent which=\"type:article\"}(+name:\"Alan Fenwick\")",
                 json["results"]["solr_params"]["fq"]
  end

  test "should load document details" do
    get search_url(
          format: "json",
          "fields[]": "doi",
          "values[]": "10.1371/journal.pntd.0000534"
        )
    json = JSON.parse(@response.body)

    assert_response :success
    assert_equal 1, json["results"]["num_hits"]

    doc = json["results"]["documents"][0]
    refute_nil doc
    assert_equal "10.1371/journal.pntd.0000534", doc["doi"]
    assert_equal "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
                 doc["license"]
    assert_equal 10, doc["authors"].size
    assert_equal "Wenbao Zhang", doc["authors"][0]["name"]
    assert_equal "Zhuangzhi", doc["authors"][1]["first"]
    assert_equal "PLoS Neglected Tropical Diseases", doc["journal"]
    assert_equal "3", doc["volume"]
  end
end
