# frozen_string_literal: true

require "test_helper"

class UserTest < ActiveSupport::TestCase
  # Import ActiveModel's own test cases
  def setup
    @model = Document.new
  end
  include ActiveModel::Lint::Tests

  test "should be invalid with no id" do
    doc = build(:document, id: nil)

    refute doc.valid?
  end

  test "should be valid with id" do
    doc = build(:document)

    assert doc.valid?
  end

  test "should create GlobalIDs" do
    doc = Document.find("doi:10.1371/journal.pntd.0000534")

    expected =
      GlobalID.new(
        "gid://sciveyor/Document/doi%3A10.1371%2Fjournal.pntd.0000534"
      )
    assert_equal expected, doc.to_global_id
  end

  test "should look up from GlobalID" do
    doc = Document.find("doi:10.1371/journal.pntd.0000534")
    doc2 =
      GlobalID::Locator.locate "gid://r-letters/Document/doi%3A10.1371%2Fjournal.pntd.0000534"

    assert_equal doc.id, doc2.id
    assert_equal doc.title, doc2.title
  end

  test "should find a single document" do
    refute_nil Document.find("doi:10.1371/journal.pntd.0000534")
  end

  test "find should raise when missing document" do
    assert_raises(ActiveRecord::RecordNotFound) { Document.find("fail") }
  end

  test "find should raise when Solr times out" do
    stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout
    assert_raises(Sciveyor::Solr::Connection::Error) { Document.find("fail") }
  end

  test "find_by should work with one document" do
    refute_nil Document.find_by(id: "doi:10.1371/journal.pntd.0000534")
  end

  test "find_by should return nil for no documents" do
    assert_nil Document.find_by(id: "fail")
  end

  test "find_by should work with other fields" do
    refute_nil Document.find_by(authorsLast: "Fenwick")
  end

  test "find_by! should raise for no documents" do
    assert_raises(ActiveRecord::RecordNotFound) do
      Document.find_by!(id: "fail")
    end
  end

  test "find_by! should raise when Solr times out" do
    stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout
    assert_raises(Sciveyor::Solr::Connection::Error) do
      Document.find_by!(id: "fail")
    end
  end

  test "should load id for single document" do
    assert_equal "doi:10.1371/journal.pntd.0000534",
                 Document.find("doi:10.1371/journal.pntd.0000534").id
  end

  test "should not load term vectors for single document" do
    assert_nil Document.find("doi:10.1371/journal.pntd.0000534").term_vectors
  end

  test "should get attributes for a set of documents" do
    docs = Sciveyor::Solr::Connection.search(q: "*:*", sort: "id asc").documents

    assert_equal "doi:10.1371/journal.pntd.0000001", docs[0].id
    assert_equal "10.1371/journal.pntd.0000033", docs[3].doi
    assert_equal "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
                 docs[0].license
    assert_equal "https://plos.org/terms-of-use/", docs[2].licenseUrl
    assert_equal "A Schistosomiasis Research Agenda", docs[2].title
    assert_equal "PLoS Neglected Tropical Diseases", docs[5].journal
    assert_equal Date.new(2007, 10, 1), docs[5].date
    assert_equal "1", docs[7].volume
    assert_equal "e65", docs[8].pages

    authors = [
      "Geneviève Milon",
      "Ghislaine Guigon",
      "Gloria Morizot",
      "Hervé Lecoeur",
      "Pierre Buffet",
      "Sophie Goyard",
      "Thierry Lang"
    ]
    assert_equal authors, docs[4].authors.map(&:name).sort
  end

  test "should nil out empty attributes" do
    doc = build(:document, volume: "")

    assert_nil doc.volume
  end

  test "should nil out blank attributes" do
    doc = build(:document, number: "   ")

    assert_nil doc.number
  end

  test "should parse basic start and end pages" do
    doc = build(:document, pages: "1227-1238")

    assert_equal "1227", doc.start_page
    assert_equal "1238", doc.end_page
  end

  test "should parse abbreviated start and end pages" do
    doc = build(:document, pages: "1483-92")

    assert_equal "1483", doc.start_page
    assert_equal "1492", doc.end_page
  end

  test "should parse non-ranged start and end pages" do
    doc = build(:document, pages: "e1234")

    assert_equal "e1234", doc.start_page
    assert_nil doc.end_page
  end

  test "should parse term vectors correctly" do
    doc = Document.find("doi:10.1371/journal.pntd.0000534", term_vectors: true)

    refute_nil doc.term_vectors

    assert_equal 1, doc.term_vectors["decrease"][:tf]
    assert_equal 10, doc.term_vectors["hyperendemic"][:positions][0]
    assert_equal 54, doc.term_vectors["remote"][:df]
    assert_nil doc.term_vectors["zuzax"]
  end
end
