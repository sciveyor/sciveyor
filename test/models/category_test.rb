# frozen_string_literal: true

require "test_helper"

class CategoryTest < ActiveSupport::TestCase
  test "to_s should work" do
    cat = build(:category)

    assert_includes cat.to_s, cat.name
  end

  test "active should return categories when active" do
    cat = create(:category)
    params = ActionController::Parameters.new(categories: [cat.to_param])
    search_params = Sciveyor::Solr::Params.new(params: params)

    assert_includes Category.active(search_params), cat
  end

  test "active should return nothing when inactive" do
    params = ActionController::Parameters.new
    search_params = Sciveyor::Solr::Params.new(params: params)

    assert_empty Category.active(search_params)
  end

  test "should return enabled when enabled" do
    cat = create(:category)
    params = ActionController::Parameters.new(categories: [cat.to_param])
    search_params = Sciveyor::Solr::Params.new(params: params)

    assert cat.enabled?(search_params)
  end

  test "should return disabled when disabled" do
    cat = create(:category)
    params = ActionController::Parameters.new
    search_params = Sciveyor::Solr::Params.new(params: params)

    refute cat.enabled?(search_params)
  end

  test "should return toggled params when enabled" do
    cat = create(:category)
    params = ActionController::Parameters.new(categories: [cat.to_param])
    search_params = Sciveyor::Solr::Params.new(params: params)
    ret = cat.toggle_search_params(search_params)

    assert_nil ret.categories
  end

  test "should return toggled params when disabled" do
    cat = create(:category)
    params = ActionController::Parameters.new
    search_params = Sciveyor::Solr::Params.new(params: params)
    ret = cat.toggle_search_params(search_params)

    assert_equal [cat.id], ret.categories
  end
end
