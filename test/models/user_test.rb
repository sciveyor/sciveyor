# frozen_string_literal: true

require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "should be invalid with no name" do
    user = build_stubbed(:user, name: nil)

    refute user.valid?
  end

  test "should be invalid with no email" do
    user = build_stubbed(:user, email: nil)

    refute user.valid?
  end

  test "should be invalid with duplicate email" do
    dupe = create(:user)
    user = build(:user, email: dupe.email)

    refute user.valid?
  end

  test "should be invalid with bad email" do
    user = build(:user, email: "asdf-not-an-email.com")

    refute user.valid?
  end

  test "should be invalid with invalid language" do
    user = build_stubbed(:user, language: "notalocaleCODE123")

    refute user.valid?
  end

  test "should be valid with good attributes" do
    user = create(:user)

    assert user.valid?
  end

  test "to_s should work" do
    user = build(:user)

    assert_includes user.to_s, user.email
  end

  test "dataset add/remove should work" do
    user = create(:user)
    dataset = create(:dataset)

    assert_equal 0, user.datasets.count

    user.add_dataset(dataset, "name")
    assert_equal 1, user.datasets.count
    assert_equal dataset.uuid, user.datasets.first.uuid
    assert_equal dataset, user.datasets_hash["name"]

    user.remove_dataset(dataset)
    assert_equal 0, user.datasets.count
    assert_nil user.datasets_hash["name"]
  end

  test "dataset double-remove should do nothing" do
    user = create(:user)
    dataset = create(:dataset)

    user.add_dataset(dataset, "name")
    assert_equal 1, user.datasets.count

    user.remove_dataset(dataset)
    assert_equal 0, user.datasets.count

    user.remove_dataset(dataset)
    assert_equal 0, user.datasets.count
  end

  test "dataset double-add should do nothing" do
    user = create(:user)
    dataset = create(:dataset)

    user.add_dataset(dataset, "name")
    user.add_dataset(dataset, "name_two")

    assert_equal 1, user.datasets.count
    assert_nil user.datasets_hash["name_two"]
  end

  test "dataset nil add should do nothing" do
    user = create(:user)
    user.add_dataset(nil, "name")

    assert_equal 0, user.datasets.count
  end

  test "dataset nil delete should do nothing" do
    user = create(:user)
    dataset = create(:dataset)
    user.add_dataset(dataset, "name")

    user.remove_dataset(nil)

    assert_equal 1, user.datasets.count
    assert_equal dataset, user.datasets_hash["name"]
  end

  test "can_export should be false if just exported" do
    user = create(:user, export_requested_at: Time.current)

    refute user.can_export?
  end

  test "can_export should be true if never exported" do
    user = create(:user)

    assert user.can_export?
  end

  test "can_export should be true if exported long ago" do
    user = create(:user, export_requested_at: 3.weeks.ago)

    assert user.can_export?
  end

  test "should not get workflow dataset for too-large" do
    user = create(:user, workflow_datasets: [])

    assert_raises(ActiveRecord::RecordNotFound) { user.workflow_dataset(0) }
  end

  test "should not get workflow dataset for invalid" do
    user = create(:user, workflow_datasets: ["999999"])

    assert_raises(ActiveRecord::RecordNotFound) { user.workflow_dataset(0) }
  end

  test "should get workflow dataset" do
    user = create(:user)
    dataset = create(:dataset)
    user.workflow_datasets = [dataset.uuid]
    user.save

    assert_equal dataset, user.workflow_dataset(0)
  end
end
