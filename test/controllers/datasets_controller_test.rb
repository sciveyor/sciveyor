# frozen_string_literal: true

require "test_helper"

class DatasetsControllerTest < ActionDispatch::IntegrationTest
  test "should redirect index when not logged in" do
    get datasets_url

    assert_redirected_to root_url
  end

  test "should get index" do
    sign_in create(:user)

    get datasets_url

    assert_response :success
  end

  test "should get index via XHR" do
    sign_in create(:user)

    get datasets_url, xhr: true

    assert_response :success
  end

  test "should get new" do
    sign_in create(:user)

    get new_dataset_url

    assert_response :success
  end

  test "should create with no workflow active" do
    user = create(:user)
    sign_in user

    params = ActionController::Parameters.new
    search_params = Sciveyor::Solr::Params.new(params: params)
    hash = search_params.to_dataset
    hash[:name] = "New Dataset"

    post datasets_url(hash)

    assert_equal 1, user.datasets.size

    d = user.datasets.first
    assert_equal "New Dataset", d.name_for(user)
    assert_equal 1, d.q.length
    assert_equal "*:*", d.q[0]

    assert_redirected_to datasets_path
  end

  test "should create with an active workflow" do
    user = create(:user)
    user.workflow_active = true
    user.workflow_class = "ArticleDatesJob"
    user.save!
    sign_in user

    post datasets_url(name: "New Dataset")

    assert_redirected_to workflow_activate_path("ArticleDatesJob")
    refute_nil flash[:success]

    assert_equal 1, user.reload.workflow_datasets.count
    assert_equal user.datasets.first.uuid, user.workflow_datasets[0]
  end

  test "should get show" do
    user = create(:user)
    dataset = create(:full_dataset)
    user.add_dataset(dataset, "dataset test name")
    sign_in user

    get dataset_url(id: dataset.uuid)

    assert_response :success
  end

  test "should delete dataset" do
    user = create(:user)
    dataset = create(:full_dataset)
    sign_in user

    delete dataset_url(id: dataset.uuid)

    assert_redirected_to datasets_url
    assert_empty user.datasets
  end

  test "should fail to update with an invalid document" do
    user = create(:user)
    dataset = create(:full_dataset)
    sign_in user

    patch dataset_url(id: dataset.uuid, doc_id: "fail")

    assert_response 404
  end

  test "should patch update without existing" do
    user = create(:user)
    dataset = create(:full_dataset)
    user.add_dataset(dataset, "test")
    sign_in user

    assert_difference "user.datasets_hash[\"test\"].document_count", 1 do
      patch dataset_url(id: dataset.uuid, doc_id: generate(:working_id))
    end

    assert_redirected_to dataset_url(user.datasets_hash["test"])
  end

  test "should patch update returning existing" do
    user = create(:user)
    dataset = create(:full_dataset, num_docs: 2)
    user.add_dataset(dataset, "test")
    sign_in user

    next_doi = generate(:working_id)
    query = dataset.q[0].delete_suffix(")")
    query += " OR \"#{next_doi}\")"

    extant_added = create(:dataset, q: [query])

    assert_equal dataset.uuid, user.datasets_hash["test"].uuid

    patch dataset_url(id: dataset.uuid, doc_id: next_doi)
    assert_redirected_to dataset_url(extant_added)

    assert_equal extant_added.uuid, user.datasets_hash["test"].uuid
  end
end
