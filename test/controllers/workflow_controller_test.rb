# frozen_string_literal: true

require "test_helper"

# Mock job class for the workflow controller
module Sciveyor
  module Jobs
    class Workflow < Base
      def perform(task, options)
      end
    end
  end
end

class WorkflowControllerTest < ActionDispatch::IntegrationTest
  test "should get index when logged in" do
    sign_in create(:user)

    get workflow_url

    assert_response :success
  end

  test "should get index when not logged in" do
    get workflow_url

    assert_response :success
  end

  test "should get index if Solr fails" do
    stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout

    get workflow_url

    assert_response :success
  end

  test "should get info" do
    user = create(:user)
    sign_in user

    get workflow_info_url(class: "ArticleDates")

    assert_response :success
  end

  test "should get start" do
    user = create(:user)
    sign_in user

    get workflow_start_url

    assert_response :success
  end

  test "should get stop" do
    user = create(:user, workflow_active: true, workflow_class: "ArticleDates")
    sign_in user
    dataset = create(:dataset)
    user.workflow_datasets = [dataset.uuid]
    user.save

    get workflow_stop_url

    user.reload
    refute user.workflow_active
    assert_nil user.workflow_class
    assert_empty user.workflow_datasets
  end

  test "should get activate with no datasets" do
    user = create(:user)
    sign_in user

    get workflow_activate_url(class: "ArticleDates")

    user.reload
    assert user.workflow_active
    assert_equal "ArticleDates", user.workflow_class
    assert_empty user.workflow_datasets
  end

  test "should get activate and link a dataset" do
    user = create(:user)
    sign_in user
    dataset = create(:dataset)

    get workflow_activate_url(
          class: "ArticleDates",
          link_dataset_id: dataset.uuid
        )

    user.reload
    assert user.workflow_active
    assert_equal "ArticleDates", user.workflow_class
    assert_equal [dataset.uuid], user.workflow_datasets
  end

  test "should get activate and unlink one dataset" do
    user = create(:user, workflow_active: true, workflow_class: "ArticleDates")
    sign_in user
    dataset = create(:dataset)
    user.workflow_datasets = [dataset.uuid]
    user.save

    get workflow_activate_url(
          class: "ArticleDates",
          unlink_dataset_id: dataset.uuid
        )

    user.reload
    assert_empty user.workflow_datasets
  end

  test "should get activate and unlink one of two datasets" do
    user = create(:user, workflow_active: true, workflow_class: "CraigZeta")
    sign_in user
    dataset = create(:dataset)
    dataset2 = create(:dataset)
    user.workflow_datasets = [dataset.uuid, dataset2.uuid]
    user.save

    get workflow_activate_url(
          class: "CraigZeta",
          unlink_dataset_id: dataset2.uuid
        )

    user.reload
    assert_equal [dataset.uuid], user.workflow_datasets
  end

  test "should not get params for invalid class" do
    user = create(:user, workflow_active: true, workflow_class: "ThisIsNoClass")
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    get workflow_options_url(class: "ThisIsNoClass")

    assert_response 500
  end

  test "should not get params for Base class" do
    user = create(:user, workflow_active: true, workflow_class: "Base")
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    get workflow_options_url(class: "Base")

    assert_response 500
  end

  test "should get params for one-dataset class" do
    user =
      create(:user, workflow_active: true, workflow_class: "ExportCitations")
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    get workflow_options_url(class: "ExportCitations")

    assert_response :success
  end

  # At the moment, we have no two-dataset classes that have a params view,
  # and thus this test can't actually test any real code.
  #
  # test 'should get params for two-dataset class' do
  #   user = create(:user, workflow_active: true, workflow_class: 'CraigZeta')
  #   dataset = create(:full_dataset)
  #   dataset2 = create(:full_dataset)
  #   sign_in user
  #   user.workflow_datasets = [dataset.uuid, dataset2.uuid]
  #   user.save

  #   get workflow_options_url(class: 'CraigZeta')

  #   assert_response :success
  # end

  test "should not get params for two-dataset class with one missing" do
    user = create(:user, workflow_active: true, workflow_class: "CraigZeta")
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    get workflow_options_url(class: "CraigZeta")

    assert_response 500
  end

  test "should not get run with invalid class" do
    user = create(:user, workflow_active: true, workflow_class: "ThisIsNoClass")
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    post workflow_run_url(class: "ThisIsNoClass")

    assert_response 500
  end

  test "should not get run with Base class" do
    user = create(:user, workflow_active: true, workflow_class: "Base")
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    post workflow_run_url(class: "Base")

    assert_response 500
  end

  test "should get run when valid class and no params passed" do
    user = create(:user)
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    post workflow_run_url(class: "ProperNames")

    assert_redirected_to root_url
    refute user.reload.workflow_active
    assert_nil user.workflow_class
    assert_empty user.workflow_datasets

    refute_empty user.reload.tasks
    refute_empty Job.where(task: user.tasks.first)
  end

  test "should get run when valid class and params passed" do
    user = create(:user)
    dataset = create(:full_dataset)
    sign_in user
    user.workflow_datasets = [dataset.uuid]
    user.save

    post workflow_run_url(
           class: "ExportCitations",
           job_params: {
             format: "bibtex"
           }
         )

    assert_redirected_to root_url
    refute user.reload.workflow_active
    assert_nil user.workflow_class
    assert_empty user.workflow_datasets

    refute_empty user.reload.tasks
    refute_empty Job.where(task: user.tasks.first)
  end

  test "should get fetch" do
    sign_in create(:user)

    get workflow_fetch_url

    assert_response :success
  end

  test "should terminate when asked to via fetch" do
    user = create(:user)
    user2 = create(:user)
    sign_in user

    dataset = create(:dataset)
    dataset2 = create(:dataset)

    finished =
      create(
        :task,
        user: user,
        datasets: [dataset],
        type: "Workflow",
        finished: true,
        failed: false,
        finished_at: Time.current
      )
    working = create(:task, user: user, datasets: [dataset], type: "Workflow")
    failed =
      create(
        :task,
        user: user,
        datasets: [dataset],
        type: "Workflow",
        finished: true,
        failed: true,
        finished_at: Time.current
      )

    other =
      create(
        :task,
        user: user2,
        datasets: [dataset2],
        type: "Workflow",
        finished: true,
        finished_at: Time.current
      )

    get workflow_fetch_url(terminate: true)

    # Delete all the finished or pending tasks
    refute Task.exists?(working.id)

    # Leave everything else alone
    assert Task.exists?(finished.id)
    assert Task.exists?(failed.id)
    assert Task.exists?(other.id)

    assert_redirected_to root_url
    refute_nil flash[:alert]
  end

  test "should get fetch_load" do
    sign_in create(:user)

    get workflow_fetch_load_url

    assert_response :success
    refute_includes @response.body, "<html"
  end

  test "should not get view when invalid task passed" do
    user = create(:user)
    sign_in user

    get view_task_url(id: "12345678", template: "test")

    assert_response 404
  end

  test "should not get view when invalid template passed" do
    user = create(:user)
    dataset = create(:full_dataset)
    task =
      create(:task, user: user, datasets: [dataset], type: "ExportCitations")
    sign_in user

    get view_task_url(id: task.to_param, template: "notaview")

    assert_response 500
  end

  test "should get view" do
    user = create(:user)
    dataset = create(:full_dataset)
    task =
      create(:task, user: user, datasets: [dataset], type: "ExportCitations")
    sign_in user

    get view_task_url(id: task.to_param, template: "_info")

    assert_response :success
  end

  test "should not destroy for invalid id" do
    user = create(:user)
    sign_in user

    delete destroy_task_url(id: "12345678")

    assert_response 404
  end

  test "should delete task" do
    user = create(:user)
    dataset = create(:full_dataset)
    task =
      create(:task, user: user, datasets: [dataset], type: "ExportCitations")
    sign_in user

    assert_difference("Task.count", -1) do
      delete destroy_task_url(id: task.to_param)
    end

    assert_redirected_to(workflow_fetch_path)
    refute_nil flash[:success]
  end
end
