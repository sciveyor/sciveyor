# frozen_string_literal: true

require "test_helper"

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  test "should not get export as HTML" do
    get documents_export_url(id: generate(:working_id))

    assert_response 406
  end

  Sciveyor::Documents::Serializers::Base.available.each do |k|
    test "should export in #{k} format" do
      get documents_export_url(id: generate(:working_id), format: k.to_s)

      assert_valid_download(Mime::Type.lookup_by_extension(k).to_s, @response)
    end
  end

  test "should not export an invalid format" do
    get documents_export_url(id: generate(:working_id), format: "csv")

    assert_response 406
  end
end
